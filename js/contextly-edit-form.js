(function ($) {

  var links = $([]);

  var onSettingsLoaded = function () {
    links
      .unbind('click.contextly')
      .bind('click.contextly', function () {
        // Open popup and just reload settings on any message from the server.
        Contextly.editor.open('snippet');
        return false;
      })
      .text(Drupal.t('Open Contextly editor'));
  };

  var onSettingsFailed = function () {
    links
      .unbind('click.contextly')
      .bind('click.contextly', function () {
        Contextly.editor.loadSettings();
        return false;
      })
      .text(Drupal.t('Unable to load settings from Contextly. Click to try again.'));
  };

  var onSettingsLoading = function () {
    links
      .unbind('click.contextly')
      .bind('click.contextly', function () {
        return false;
      })
      .text(Drupal.t('Loading...'));
  };

  Drupal.behaviors.contextlyEditForm = {
    attach: function (context, settings) {
      // Search for the links in the context and add them to the global list
      // of links.
      var contextLinks = $('a.contextly-popup-link', context).once('contextly-popup-link');
      links = links.add(contextLinks);

      // React to the window events. Attach only once for the page.
      $('body', context).once('contextly-edit-form', function() {
        $(window).bind({
          contextlySettingsLoading: onSettingsLoading,
          contextlySettingsLoaded: onSettingsLoaded,
          contextlySettingsFailed: onSettingsFailed
        });

        // In case the popup has been attached before this one we need to check
        // its stage and change the state of the links.
        if (Contextly.editor.isLoading) {
          links.each(onSettingsLoading);
        }
      });
    },

    detach: function(context) {
      // Remove the links found in the context from the global list of links.
      var contextLinks = $('a.contextly-popup-link', context);
      links = links.not(contextLinks);
    }
  };

})(jQuery);
