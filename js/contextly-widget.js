(function ($) {

  Contextly = window.Contextly || {};

  Contextly.WidgetType = {
    SNIPPET: 'snippet',
    SIDEBAR: 'sidebar'
  };

  Contextly.WidgetDisplayType = {
    TABS: 'tabs',
    BLOCKS: 'blocks',
    BLOCKS2: 'blocks2',
    FLOAT: 'float',
    DEFAULT: 'default'
  };

  Contextly.LinkType = {
    PREVIOUS: 'previous',
    RECENT: 'recent',
    WEB: 'web',
    INTERESTING: 'interesting',
    CUSTOM: 'custom',
    PROMO: 'sticky'
  };

  /**
   * Generates widget class based on settings.
   */
  Contextly.WidgetFactory = {

    /**
     * Returns widget instance depending on the passed data or null if data is
     * empty or the widget type is unknown.
     *
     * @param widgetData
     *   Widget data returned by Contextly service.
     * @param moduleSettings
     *   Global settings passed from the module.
     *
     * @returns {Contextly.BaseWidget|null}
     */
    getWidget: function(widgetData, moduleSettings) {
      if (widgetData.type) {
        switch (widgetData.type) {
          case 'sidebar':
            return new Contextly.SidebarWidget(widgetData, moduleSettings);

          case 'normal':
            if (widgetData.settings && widgetData.settings.display_type) {
              switch (widgetData.settings.display_type) {
                case Contextly.WidgetDisplayType.TABS:
                  return new Contextly.TabsWidget(widgetData, moduleSettings);

                case Contextly.WidgetDisplayType.BLOCKS:
                  return new Contextly.BlocksWidget(widgetData, moduleSettings);

                case Contextly.WidgetDisplayType.BLOCKS2:
                  return new Contextly.Blocks2Widget(widgetData, moduleSettings);

                case Contextly.WidgetDisplayType.FLOAT:
                  return new Contextly.FloatWidget(widgetData, moduleSettings);

                case Contextly.WidgetDisplayType.DEFAULT:
                  return new Contextly.TextWidget(widgetData, moduleSettings);
              }
            }
            break;
        }
      }

      return null;
    }

  };

  /**
   * Base class for all kinds of widgets.
   *
   * @class
   */
  Contextly.BaseWidget = Contextly.createClass( /** @lends Contextly.BaseWidget.prototype */ {

    /**
     * Widget constructor.
     *
     * @param widgetData
     *   Widget data and settings received from Contextly.
     * @param moduleSettings
     *   Global settings passed from the module.
     */
    construct: function (widgetData, moduleSettings) {
      this.widget = widgetData;
      this.settings = widgetData.settings;
      this.isIE7 = ($.browser.msie && parseFloat($.browser.version) < 8);
      this.version = moduleSettings.version;
      this.mode = moduleSettings.mode;
      this.widgetStylesURL = moduleSettings.widgetStylesURL;

      // Basic state-dependent classes.
      this.classes = {
        empty: ['contextly-empty'],
        rendered: ['contextly-loaded']
      };
    },

    /**
     * Returns the widget type.
     *
     * @returns {string}
     */
    getWidgetType: function() {
      return Contextly.WidgetType.SNIPPET;
    },

    /**
     * Builds and returns HTML of the widget.
     *
     * @function
     *
     * @returns {string}
     */
    buildWidget: Contextly.abstractMethod(),

    buildAttributes: function(attributes) {
      var result = '';
      for (var key in attributes) {
        result += ' ' + key + '="' + this.escape(attributes[key]) + '"';
      }
      return result;
    },

    /**
     * Returns the anchor tag for passed normal link.
     *
     * @param link
     * @param content
     *
     * @returns {string}
     */
    buildLinkTag: function (link, content) {
      var attributes = {
        target:  '_blank',
        href: link.native_url,
        title: link.title,
        'data-contextly-url': link.url
      };

      var classes = ['title', 'module-contextly', 'contextly_dynamic_url'];
      if (this.settings.utm_enable) {
        classes.push('contextly_ga_link');
        attributes['data-contextly-link-type'] = link.type;
      }

      attributes['class'] = classes.join(' ');

      return '<a' + this.buildAttributes(attributes) + '>' +
        content +
        "</a>" +
        this.buildLinkTagIE7Fix();
    },

    buildVideoLinkTag: function (link, content) {
      var attributes = {
        rel: 'contextly-video-link',
        href: link.native_url,
        title: link.title,
        'data-contextly-url': link.url
      };

      var classes = ['title', 'module-contextly'];
      if (this.settings.utm_enable) {
        classes.push('contextly_ga_link');
        attributes['data-contextly-link-type'] = link.type;
      }

      attributes['class'] = classes.join(' ');

      return '<a' + this.buildAttributes(attributes) + '>' +
        content +
        "</a>" +
        this.buildLinkTagIE7Fix();
    },

    buildLogoLink: function() {
      var div = '';

      div += "<div class=\"pluginauthor my_modal_open\"><span>Powered by</span></div>";
      div += "<div id=\"my_modal\" class=\"well\" style=\"display:none;margin:1em;\">";
      div += "<a href=\"#\" class=\"my_modal_close\" ></a>";
      div += "<div id='brandpopupcontainer'>";
      div += "<span id=\"brandpoplogo\"></span><span id='brandpopupperbg'></span><div id='brandpoptext'>";
      div += "Contextly recommends interesting and related stories using a unique combination of algorithms and editorial choices.<br><br>";
      div += "Publishers or advertisers who would like to learn more about Contextly can contact us&nbsp;";
      div += "<a href=\"http://contextly.com/sign-up/publishers/\" target=\"_blank\">here</a>.<br><br>";
      div += "We respect ";
      div += "<a href=\"http://contextly.com/privacy/\" target=\"_blank\">readers' privacy</a>.&nbsp;";
      div += "</div></div>";
      div += "<span id='brandpopsymbol'></span>";
      div += "</div>";

      return div;
    },

    attachGALinkHandlers: function(container) {
      window._gaq = window._gaq || [];
      var self = this;

      container
        .find('a.contextly_ga_link')
        .click(function() {
          var $this = $(this);
          var title = $this.attr('title');

          var category;
          var widgetType = self.getWidgetType();
          switch (widgetType) {
            case Contextly.WidgetType.SIDEBAR:
              category = 'ContextlySidebar';
              break;

            default:
              category = 'ContextlyWidget';
          }

          var action;
          var linkType = $this.attr('data-contextly-link-type');
          // TODO Cleanup this mess into switch.
          if (widgetType == Contextly.WidgetType.SIDEBAR && (linkType == Contextly.LinkType.WEB || linkType == Contextly.LinkType.PREVIOUS)) {
            action = 'ClickedRecentRelated';
          }
          else if (linkType == Contextly.LinkType.PREVIOUS) {
            action = 'ClickedPreviousRelated';
          }
          else if (linkType == Contextly.LinkType.RECENT) {
            action = 'ClickedRecentRelated';
          }
          else if (linkType == Contextly.LinkType.PROMO) {
            action = 'ClickedPromoLink';
          }
          else {
            action = 'Clicked' + linkType.charAt(0).toUpperCase() + linkType.slice(1);
          }

          window._gaq.push(['_trackEvent', category, action, title]);
        });
    },

    attachBrandingPopupHandlers: function(container) {
      container
        .find('.my_modal_open')
        .click(function () {
          $('#my_modal').popup({
            opacity: 0.3,
            fade: 300,
            keepfocus: false,
            autozindex: true,
            autoopen: true
          });
        });
    },

    buildLinkTagIE7Fix: function () {
      if (this.isIE7) {
        return '<!--[if lte ie 7]><b></b><![endif]-->';
      }
      else {
        return '';
      }
    },

    escape: function (text) {
      if (!text) {
        return '';
      }

      return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
    },

    /**
     * Returns HTML class identifying container of this widget.
     *
     * This class is not really unique on the page, because there can be
     * multiple instances of the same widget.
     *
     * @function
     *
     * @returns {string}
     */
    getWidgetIdClass: function() {
      return 'contextly-widget--' + this.widget.id;
    },

    /**
     * Returns CSS class for the widget display type.
     *
     * @returns {string}
     */
    getWidgetStyleClass: function() {
      return 'default-widget';
    },

    /**
     * Returns true if passed in section can be displayed.
     *
     * @param {string} section
     *
     * @returns {boolean}
     */
    sectionIsDisplayable: function (section) {
      if ($.inArray(section, this.settings.display_sections) == -1) {
        // Section is hidden by settings.
        return false;
      }

      return !!(this.widget.links && this.widget.links[section] && this.widget.links[section].length > 0);
    },

    /**
     * Returns base CSS URL for the widget.
     */
    getCSSTemplate: function() {
      return this.widgetStylesURL + 'widget/' + this.settings.display_type + "/template-" + this.settings.tabs_style + ".css";
    },

    /**
     * Returns IE7-specific CSS fixes URL.
     */
    getIE7CSSFix: function() {
      // Return nothing by default.
    },

    /**
     * Applies widget settings to current instance.
     */
    applySettingsCSS: function() {
      var widgetClass = this.getWidgetIdClass();
      if (!Contextly.CSSLoader.areSettingsApplied(widgetClass)) {
        var prefix = '.' + widgetClass;
        var css = this.buildSettingsCSS(prefix);
        Contextly.CSSLoader.applySettingsOnce(widgetClass, css);
      }
    },

    /**
     * Returns CSS code for settings of the widget.
     *
     * @param prefix
     *
     * @returns {string}
     */
    buildSettingsCSS: function (prefix) {
      // Returns nothing by default.
      return '';
    },

    /**
     * Returns single CSS rule.
     *
     * @param {string} prefix
     *   Selector prefix.
     * @param {string} selector
     * @param {string} property
     *   Property name.
     * @param {string} value
     *   Property value.
     *
     * @returns {string}
     */
    buildCSSRule: function (prefix, selector, property, value) {
      if (!value) {
        return "";
      }

      // TODO Escape value!
      return prefix + " " + selector + " {" + property + ": " + this.escape(value) + "}\n";
    },

    parseHexColor: function (hex) {
      hex = hex
        .replace(/^#/, '')
        .toUpperCase();

      var result = new Array(3);
      for (var i = 0; i < 3; i++) {
        var start = i * 2;
        var end = start + 2;
        var value = hex.slice(start, end);
        value = parseInt(value, 16);
        result[i] = value;
      }

      return result;
    },

    /**
     * Attaches event handlers to the container after rendering HTML to it.
     *
     * @param container
     */
    attachEventHandlers: function(container) {
      this.attachDynamicURLHandlers(container);
      if (this.settings.utm_enable) {
        this.attachGALinkHandlers(container);
      }
    },

    /**
     * Attaches handlers to dynamically replace the link URL.
     *
     * URL of the link is replaced with the one stored in data-* attribute.
     *
     * Actual URL is a redirect URL on the Contextly server. On mouse over user
     * sees the final destination and on click we pass him through the redirect
     * to get some statistics.
     *
     * @param container
     */
    attachDynamicURLHandlers: function(container) {
      container
        .find('a.contextly_dynamic_url')
        .mousedown(function () {
          var $this = $(this);
          $this.attr('href', $this.attr('data-contextly-url'));
        });
    },

    /**
     * Returns true if widget has data to display.
     *
     * @returns {boolean}
     */
    hasData: function () {
      return !!(this.widget && this.widget.links && !$.isEmptyObject(this.widget.links));
    },

    /**
     * Adds classes to the container depending on the passed in state.
     *
     * @param {string} state
     *   One of:
     *   - empty
     *   - rendered
     * @param container
     */
    addStateClasses: function(state, container) {
      if (this.classes && this.classes[state]) {
        $.each(this.classes[state], function() {
          container.addClass(this);
        });
      }
    },

    /**
     * Removes classes to the container depending on the passed in state.
     *
     * @param {string} state
     *   One of:
     *   - empty
     *   - rendered
     * @param container
     */
    removeStateClasses: function(state, container) {
      if (this.classes && this.classes[state]) {
        $.each(this.classes[state], function() {
          container.removeClass(this);
        });
      }
    },

    /**
     * Displays the widget inside passed container.
     *
     * @param container
     *   jQuery object of the container tag.
     */
    display: function(container) {
      if (!container.size()) {
        return;
      }

      if (!this.hasData()) {
        // The widget is empty. Just add classes and exit.
        this.addStateClasses('empty', container);
        return;
      }

      // Load CSS template, IE 7 fixes and apply settings CSS.
      Contextly.CSSLoader.loadOnce(this.getCSSTemplate());
      if (this.isIE7) {
        Contextly.CSSLoader.loadOnce(this.getIE7CSSFix());
      }
      this.applySettingsCSS();

      // Render HTML into the container, attach event handlers and Drupal
      // behaviors.
      var html = this.buildWidget();
      container.html(html);
      this.addStateClasses('rendered', container);
      this.attachEventHandlers(container);
      Drupal.attachBehaviors(container);
      this.applyResponsiveLayout(container);
    },

    getImageDimensions: function () {
      var imagesType = this.settings.images_type
        .replace(/^(square|letter)/, '');

      var dimensions = imagesType.split('x');
      var w = 0;
      var h = 0;

      if (dimensions.length == 2) {
        w = parseInt(dimensions[0]);
        h = parseInt(dimensions[1]);
      }

      return {width: w, height: h};
    },

    getImagesHeight: function () {
      var imageDimensions = this.getImageDimensions();
      return imageDimensions.height;
    },

    getImagesWidth: function () {
      var imageDimensions = this.getImageDimensions();
      return imageDimensions.width;
    },

    /**
     * Applies responsive styles to the widget.
     */
    applyResponsiveLayout: function(container) {
      // Do nothing by default.
    },

    /**
     * Returns function that will always be called in "this" context.
     *
     * @param func
     * @param [args]
     *   - Omit or pass falsy value to call function without arguments.
     *   - Pass true to use arguments from the function call.
     *   - Pass array to use fixed arguments.
     * @param [keepContext]
     *   Pass true to add call-time context to the top of arguments list. Has
     *   any effect only if true passed to the "args" parameter.
     */
    proxy: function(func, args, keepContext) {
      var self = this;

      if (!args) {
        return function () {
          return func.call(self);
        }
      }
      else if (args === true) {
        return function () {
          if (keepContext) {
            Array.prototype.unshift.call(arguments, this);
          }
          return func.apply(self, arguments);
        }
      }
      else {
        return function () {
          return func.apply(self, args);
        }
      }
    }

  });

  /**
   * Text-only widget.
   *
   * @class
   * @extends Contextly.BaseWidget
   */
  Contextly.TextWidget = Contextly.createClass( /** @lends Contextly.TextWidget.prototype */ {

    /**
     * Parent class.
     */
    extend: Contextly.BaseWidget,

    /**
     * @inheritDoc
     */
    buildWidget: function () {
      var div = "";
      div += '<div class="contextly_see_also ' + this.escape(this.getWidgetStyleClass()) + '">';

      var sections = this.widget.settings.display_sections;
      for (var section in sections) {
        var section_name = sections[section];
        if (this.sectionIsDisplayable(section_name)) {
          var section_header = this.widget.settings[section_name + '_subhead'];

          div += "<div class='contextly_previous'>";
          div += "<div class='contextly_subhead'><span class='contextly_subhead_title'>" + this.escape(section_header) + "</span><span class='contextly_subhead_line'></span></div>";
          div += "<ul class='link'>" + this.buildSectionLinks(section_name) + "</ul>";
          div += "</div>";
        }
      }

      div += '</div>';

      return div;
    },

    /**
     * Returns link HTML.
     *
     * @param link
     *
     * @returns {string}
     */
    buildLink: function (link) {
      // TODO Remove escaping when title will be properly filtered on the API
      // side.
      var content = this.escape(link.title);
      return '<li>' + this.buildLinkTag(link, content) + '</li>';
    },

    /**
     * Return HTML of the passed section links.
     *
     * Section must be checked if it is displayable before calling this
     * function.
     *
     * @param {string} section
     * @returns {string}
     */
    buildSectionLinks: function (section) {
      var html = '';

      for (var index in this.widget.links[section]) {
        var link = this.widget.links[section][index];
        html += this.buildLink(link);
      }

      return html;
    },

    getWidgetStyleClass: function() {
      return 'text-widget';
    },

    /**
     * @inheritDoc
     */
    buildSettingsCSS: function(prefix) {
      var s = this.settings;

      var code = '';

      if (s.css_code) {
        // TODO Escape it properly.
        code += this.escape(s.css_code);
      }

      if (s.font_family) {
        code += this.buildCSSRule(prefix, ".link", "font-family", s.font_family);
      }
      if (s.font_size) {
        code += this.buildCSSRule(prefix, ".link", "font-size", s.font_size);
      }

      if (s.color_links) {
        code += this.buildCSSRule(prefix, "a.title", "color", s.color_links);
      }

      if (s.color_background) {
        code += this.buildCSSRule(prefix, ".contextly_subhead", "background-color", s.color_background);
      }

      return code;
    }

  });

  /**
   * Full-featured HTML widget class.
   *
   * @class
   * @extends Contextly.BaseWidget
   */
  Contextly.TabsWidget = Contextly.createClass( /** @lends Contextly.TabsWidget.prototype */ {

    /**
     * Parent class.
     */
    extend: Contextly.BaseWidget,

    /**
     * Attaches event handlers to switch between tabs.
     *
     * Each tab switch triggers Contextly event.
     *
     * First available tab is activated by this function.
     *
     * @param container
     */
    attachTabHandlers: function(container) {
      var self = this;

      var tabHeaders = container
        .find('ul.linker_tabs > li[id]');
      var tabContents = container
        .find('div.linker_content[id]');

      // Activate first available tab.
      tabHeaders
        .first()
        .addClass('active');
      tabContents
        .first()
        .css({
          display: 'block'
        });

      // Attach event handlers.
      tabHeaders
        .find('a')
        .click(function () {
          var header = $(this).closest('li');
          if (header.is('.active')) {
            // Don't do anything if clicked tab is already active.
            return;
          }

          var section = header.attr('data-contextly-section');
          if (!section) {
            return;
          }

          var content_id = 'linker_content_' + section;
          var content = container.find('#' + content_id);
          if (!content.size()) {
            return;
          }

          // Activate clicked tab.
          header.addClass('active');
          content.css({
            display: 'block'
          });

          // Deactivate other tabs.
          tabHeaders
            .filter('.active')
            .not(header)
            .removeClass('active');
          tabContents
            .not(content)
            .hide();

          // Fire the event.
          Contextly.firePageEvent(self.widget.id, "switch_tab", section);
        });
    },

    /**
     * Attaches handlers for the "Show more/less" links.
     *
     * Each click on these links triggers Contextly event.
     *
     * @param container
     */
    attachShowMoreHandlers: function(container) {
      var self = this;

      container
        .find('p.show-more > a')
        .click(function() {
          var tab = $(this)
            .closest('div.linker_content')
            .attr('data-contextly-section');
          if (!tab) {
            return;
          }

          $('.li' + tab).toggleClass("show");

          var pMore = $('#pmore' + tab);
          var aMore = pMore.children('a');
          pMore.toggleClass("show");
          if (pMore.hasClass('show')) {
            aMore.text(Drupal.t('Show Less'));
          }
          else {
            aMore.text(Drupal.t('Show More'));
          }

          // Fire the event.
          Contextly.firePageEvent(self.widget.id, "show_more", tab);
        });
    },

    /**
     * @inheritDoc
     */
    attachEventHandlers: function (container) {
      Contextly.BaseWidget.prototype.attachEventHandlers.call(this, container);

      this.attachTabHandlers(container);
      this.attachShowMoreHandlers(container);
      this.attachBrandingPopupHandlers(container);
    },

    /**
     * @inheritDoc
     */
    getWidgetStyleClass: function() {
      return 'tabs-widget';
    },

    /**
     * @inheritDoc
     */
    getIE7CSSFix: function() {
      return "https://c713421.ssl.cf2.rackcdn.com/_plugin/" + this.version + "/css/template-ie-fix.css";
    },

    /**
     * @inheritDoc
     */
    buildWidget: function () {
      var sections = this.settings.display_sections;

      var headers = [],
        tabs = [];
      for (var section in sections) {
        var section_name = sections[section];

        if (this.sectionIsDisplayable(section_name)) {
          var section_key = section_name + '_subhead';
          var section_header = this.settings[ section_key ];

          var header = '<li id="linker_tab_' + Drupal.checkPlain(section_name) + '" data-contextly-section="' + Drupal.checkPlain(section_name) + '">';
          header += '<a href="javascript:;"><span>' + Drupal.checkPlain(section_header) + '</span></a>';
          header += '</li>';
          headers.push(header);

          var tab = '<div id="linker_content_' + Drupal.checkPlain(section_name) + '" class="linker_content" data-contextly-section="' + Drupal.checkPlain(section_name) + '">';
          tab += '<ul class="link' + ( this.sectionHasImages(section_name) ? ' linker_images' : '' ) + '">';
          tab += this.buildSectionLinks(section_name);
          tab += '</ul>';
          tab += '</div>';
          tabs.push(tab);
        }
      }

      var div = '';
      div += '<ul class="linker_tabs">';
      div += headers.join('');

      if (this.contextlyLogoIsDisplayable()) {
        div += "<li>" + this.buildLogoLink() + "</li>";
      }

      div += "</ul>";

      div += tabs.join('');

      div = '<div class="contextly_see_also ' + this.getWidgetStyleClass() + '">' + div + '</div>';

      return div;
    },

    /**
     * Return HTML of the passed section links.
     *
     * Section must be checked if it is displayable before calling this
     * function.
     *
     * @param {string} section
     * @returns {string}
     */
    buildSectionLinks: function (section) {
      var html = "";

      if (this.widget.links && this.widget.links[section]) {
        for (var index = 0; index < this.widget.links[section].length; index++) {
          var link = this.widget.links[section][index];

          html += '<li';
          if (parseInt(index) + 1 > this.settings.links_limit) {
            html += ' class="li' + Drupal.checkPlain(section) + '"';
          }
          html += '>';
          html += this.buildLink(link);
          html += '</li>';
        }
      }

      if (this.widget.links[ section ].length > this.settings.links_limit) {
        html += '<p class="show-more" id="pmore' + Drupal.checkPlain(section) + '"><a href="javascript:;" name="amore' + Drupal.checkPlain(section) + '">' + Drupal.t('Show More') + '</a></p>';
      }

      return html;
    },

    /**
     * Returns link HTML.
     *
     * @param link
     *
     * @returns {string}
     */
    buildLink: function (link) {
      // TODO Move to CSS file!
      var itemStyle = "padding-bottom: 5px;";

      if (link.thumbnail_url) {
        itemStyle += "height: " + this.getImagesHeight() + "px;";
      }

      var html = "<ul class='horizontal-line' style='" + itemStyle + "'>";

      if (link.thumbnail_url) {
        var imageWidth = this.getImagesWidth();
        var imageLiWidth = imageWidth + 8;
        var imageHtml = "<img src='" + this.escape(link.thumbnail_url) + "' style='width: " + imageWidth + "px !important;' />";

        html += "<li style='width: " + imageLiWidth + "px;'>" + this.buildLinkTag(link, imageHtml) + "</li>";
      }

      // TODO Remove title escaping when it will be filtered properly on the API side.
      html += "<li>" + this.buildLinkTag(link, this.escape(link.title)) + "</a>";

      if (this.widget.settings.display_link_dates && link.publish_date) {
        html += " <span class='link-pub-date'>" + this.dateTextRepresentation(link.publish_date) + "</span>";
      }

      html += this.buildLinkTagIE7Fix() + "</li>";
      html += "</ul>";

      return html;
    },

    /**
     * Returns CSS code for settings of the widget.
     *
     * @param prefix
     *
     * @returns {string}
     */
    buildSettingsCSS: function (prefix) {
      var s = this.settings;

      var code = "";

      if (s.css_code) {
        // TODO Escape it properly.
        code += this.escape(s.css_code);
      }

      if (s.font_family) {
        code += this.buildCSSRule(prefix, ".link", "font-family", s.font_family);
      }
      if (s.font_size) {
        code += this.buildCSSRule(prefix, ".link", "font-size", s.font_size);
      }

      if (s.color_background) {
        code += this.buildCSSRule(prefix, ".linker_content", "background-color", s.color_background);
        code += this.buildCSSRule(prefix, ".linker_images img", "border-color", s.color_background);
      }

      if (s.color_links) {
        code += this.buildCSSRule(prefix, ".linker_content a", "color", s.color_links);
        code += this.buildCSSRule(prefix, ".linker_content span", "color", s.color_links);
      }

      if (s.color_border) {
        code += this.buildCSSRule(prefix, ".linker_content", "border-color", s.color_border);
        code += this.buildCSSRule(prefix, ".linker_tabs li.active span", "border-color", s.color_border);
        code += this.buildCSSRule(prefix, ".linker_tabs span", "border-color", s.color_border);
      }

      if (s.color_active_tab) code += this.buildCSSRule(prefix, ".linker_tabs li.active span", "background", s.color_active_tab);

      if (s.color_inactive_tab) code += this.buildCSSRule(prefix, ".linker_tabs span", "background", s.color_inactive_tab);

      return code;
    },

    /**
     * Returns text representation of the time range between current time and
     * passed date.
     *
     * @param date
     * @returns {string}
     */
    dateTextRepresentation: function (date) {
      if (date && date.length > 4) {
        var t = date.split(/[- :]/);
        var js_date = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);

        var timestamp = js_date.getTime() / 1000;
        var difference = new Date().getTime() / 1000 - timestamp;

        var periods = new Array("sec", "min", "hour", "day", "week", "month", "year", "decade");
        var lengths = new Array("60", "60", "24", "7", "4.35", "12", "10");
        var ending;

        if (difference > 0) {
          // this was in the past
          ending = "ago";
        }
        else { // this was in the future
          return 'right now';
        }

        for (var j = 0; difference >= lengths[j]; j++) {
          difference /= lengths[j];
        }
        difference = Math.round(difference);

        if (difference != 1) {
          periods[j] += "s";
        }
        return difference + "&nbsp;" + periods[j] + "&nbsp;" + ending;
      }

      return '';
    },

    /**
     * Returns true if passed section has images.
     *
     * @param {string} section
     * @returns {boolean}
     */
    sectionHasImages: function (section) {
      var imgCount = 0;

      for (var index in this.widget.links[section]) {
        if (this.widget.links[section][index].thumbnail_url) {
          imgCount++;
        }
      }

      // TODO What about using threshold from settings?
      if (this.widget.links[ section ].length == imgCount) {
        return true;
      }
      else {
        return false;
      }
    },

    /**
     * Returns true if Contextly logo must be displayed on the
     *
     * @returns {boolean}
     */
    contextlyLogoIsDisplayable: function () {
      return !this.isMobileRequest();
    },

    /**
     * Returns true if we are on mobile device.
     *
     * @returns {boolean}
     */
    isMobileRequest: function () {
      return false; // TODO: implement me
    }

  });

  /**
   * HTML Blocks widget class.
   *
   * @class
   * @extends Contextly.TextWidget
   */
  Contextly.BlocksWidget = Contextly.createClass({

    /**
     * Parent class.
     */
    extend: Contextly.TextWidget,

    /**
     * Returns the number of links per section.
     *
     * @returns {number}
     */
    getLinksPerSectionLimit: function () {
      return 4;
    },

    /**
     * @inheritDoc
     */
    buildWidget: function () {
      var div = "";
      var value;

      div += "<div class='contextly_see_also " + this.getWidgetStyleClass() + "'>";

      var sections = this.settings.display_sections;

      div += "<div class='contextly_around_site'>";
      for (var section in sections) {
        var section_name = sections[section];

        if (this.sectionIsDisplayable(section_name)) {
          var section_key = section_name + '_subhead';
          var section_header = this.settings[ section_key ];

          div += "<div class='contextly_previous'>";
          div += "<div class='contextly_subhead'><span class='contextly_subhead_title'>" + this.escape(section_header) + "</span><span class='contextly_subhead_line'></span></div>";
          div += "<ul class='link'>" + this.buildSectionLinks(section_name) + "</ul>";
          div += "</div>";
        }
      }
      div += "</div>";

      // Branding popup
      div += this.buildLogoLink();

      return div;
    },

    /**
     * @inheritDoc
     */
    buildSectionLinks: function (section) {
      var html = '';

      var linksLimit = this.getLinksPerSectionLimit();
      for (var index in this.widget.links[section]) {
        // Blocks display type can only show 4 links.
        if (index >= linksLimit) {
          break;
        }

        var link = this.widget.links[section][index];
        html += this.buildLink(link);
      }

      return html;
    },

    /**
     * @inheritDoc
     */
    buildLink: function (link) {
      if (link.video) {
        return this.buildVideoLink(link);
      }
      else {
        return this.buildNormalLink(link);
      }
    },

    /**
     * Returns video link HTML.
     *
     * @param link
     *
     * @returns {string}
     */
    buildVideoLink: function(link) {
      return "<li>" + this.buildVideoLinkTag(link, this.buildLinkContent(link)) + "</li>";
    },

    /**
     * Returns normal link HTML.
     *
     * @param link
     *
     * @returns {string}
     */
    buildNormalLink: function(link) {
      return "<li>" + this.buildLinkTag(link, this.buildLinkContent(link)) + "</li>";
    },

    /**
     * Returns inner HTML of the link (both video and normal).
     *
     * @param link
     */
    buildLinkContent: function(link) {
      // TODO Remove escaping when HTML will be filtered by API.
      var html = "<p class='link'><span>" + this.escape(link.title) + "</span></p>";
      if (link.thumbnail_url) {
        html += "<img src='" + this.escape(link.thumbnail_url) + "' />";
      }

      return html;
    },

    /**
     * @inheritDoc
     */
    buildSettingsCSS: function (prefix) {
      var s = this.settings;

      var code = '';

      if (s.css_code) {
        // TODO Escape it properly.
        code += this.escape(s.css_code);
      }

      if (s.font_family) {
        code += this.buildCSSRule(prefix, ".link", "font-family", s.font_family);
      }
      if (s.font_size) {
        code += this.buildCSSRule(prefix, ".link", "font-size", s.font_size);
      }

      if (s.color_links) {
        code += this.buildCSSRule(prefix, ".link span", "color", s.color_links);
      }

      if (s.color_background) {
        code += this.buildCSSRule(prefix, ".contextly_subhead", "background-color", s.color_background);
      }

      if (s.color_border) {
        var rgb = this.parseHexColor(s.color_border);
        if (rgb.length == 3) {
          code += this.buildCSSRule(prefix, ".blocks-widget li p", "background", "rgba(" + rgb.join(', ') + ", 0.5)");
        }
      }

      return code;
    },

    /**
     * @inheritDoc
     */
    attachEventHandlers: function(container) {
      Contextly.BaseWidget.prototype.attachEventHandlers.call(this, container);

      this.attachVideoLinkHanlders(container);
      this.attachBrandingPopupHandlers(container);
      this.attachResponsiveLayoutHandlers(container);

      // Attach to this class only, child classes don't need that.
      if (this.settings.display_type === Contextly.WidgetDisplayType.BLOCKS) {
        this.attachTitleHeightHandlers(container);
      }
    },

    /**
     * Attaches handlers to the video links.
     *
     * Video URLs
     *
     * @param container
     */
    attachVideoLinkHanlders: function(container) {
      container
        .find('a[rel="contextly-video-link"]')
        .click(function (e) {
          e.preventDefault();

          var contextly_url = $(this).attr('data-contextly-url');
          Contextly.MainServerAjaxClient.call(contextly_url);
        })
        .prettyPhoto({
          animation_speed: 'fast',
          slideshow: 10000,
          hideflash: true
        });
    },

    attachResponsiveLayoutHandlers: function(container) {
      $(window).bind('resize.contextlyWidget', this.proxy(this.applyResponsiveLayout, [container]));
    },

    attachTitleHeightHandlers: function (container) {
      container
        .find('li a')
        .bind('mouseenter.contextlyWidget', function() {
          var $this = $(this);
          $this.addClass('heightauto');
          var textHeight = $this
            .find('p span')
            .height();
          if (textHeight > 50) {
            $this
              .find('p')
              .css("height", textHeight);
          }
        })
        .bind('mouseleave.contextlyWidget', function() {
          var $this = $(this);
          $this.removeClass('heightauto');
          $this
            .find('p')
            .css("height", "");
        });
    },

    getWidgetStyleClass: function() {
      return 'blocks-widget';
    },

    applyResponsiveLayout: function (container) {
      // TODO Make it configurable.
      var limit = 500;

      var widgetWidth = container.width();
      var items = container.find('li');

      // TODO Move styles to CSS and only trigger the class.
      if (widgetWidth <= limit) {
        items.css({
          "width": "153px",
          "margin-left": "6.4%",
          "margin-bottom": "5%"
        });
      }
      else {
        items.css({
          "width": "22%",
          "margin-left": "2.4%",
          "margin-bottom": "2.2%"
        });
      }
    }

  });

  /**
   * HTML Blocks2 widget class.
   *
   * @class
   * @extends Contextly.TextWidget
   */
  Contextly.Blocks2Widget = Contextly.createClass({

    /**
     * Parent class.
     */
    extend: Contextly.BlocksWidget,

    getWidgetStyleClass: function() {
      return 'blocks-widget2';
    },

    buildLinkContent: function(link) {
      var html = "";

      if (link.thumbnail_url) {
        html += "<img src='" + this.escape(link.thumbnail_url) + "' />";

        if (link.video) {
          html = "<div class='playbutton-wrapper'>" + html + "</div>";
        }
      }

      // TODO Remove title escaping when HTML will be filtered by API.
      html += "<p class='link'><span>" + this.escape(link.title) + "</span></p>";

      return html;
    },

    applyResponsiveLayout: function (container) {
      // TODO Make it configurable.
      var limit = 350;

      var widgetWidth = container.width();
      var items = container.find('li');
      var images = items.find('img');
      var paragraphs = items.find('p');

      // TODO Move styles to CSS and only trigger the class.
      if (widgetWidth <= limit) {
        items.css({
          "width": "100%",
          "max-width": "100%"
        });
        images.css("width", "30%");
        paragraphs.css({
          "width": "60%",
          "margin-top": 0
        });
      }
      else {
        items.css({
          "width": "23%",
          "max-width": 160
        });
        images.css("width", "94%");
        paragraphs.css({
          "width": "94%",
          "margin-top": 5
        });
      }
    }

  });

  /**
   * HTML Float widget class.
   *
   * @class
   * @extends Contextly.BlocksWidget
   */
  Contextly.FloatWidget = Contextly.createClass({

    /**
     * Parent class.
     */
    extend: Contextly.BlocksWidget,

    getWidgetStyleClass: function() {
      return 'float-widget';
    },

    getLinksPerSectionLimit: function() {
      return this.settings.links_limit;
    },

    buildLinkContent: function (link) {
      var html = "";
      if (link.thumbnail_url) {
        html += "<img src='" + this.escape(link.thumbnail_url) + "' />";
        if (link.video) {
          html = "<div class='playbutton-wrapper'>" + html + "</div>";
        }
      }

      var textWidth = this.getImagesWidth() + 10;
      // TODO Remove title escaping when title will be properly escaped.
      html += "<p class='link' style='width: " + textWidth + "px;'><span>" + this.escape(link.title) + "</span></p>";

      return html;
    },

    buildSettingsCSS: function(prefix) {
      var s = this.settings;

      var code = '';

      if (s.css_code) {
        // TODO Escape it properly.
        code += this.escape(s.css_code);
      }

      if (s.font_family) {
        code += this.buildCSSRule(prefix, ".link", "font-family", s.font_family);
      }
      if (s.font_size) {
        code += this.buildCSSRule(prefix, ".link", "font-size", s.font_size);
      }

      if (s.color_links) {
        code += this.buildCSSRule(prefix, ".link span", "color", s.color_links);
      }

      return code;
    },

    applyResponsiveLayout: function (container) {
      // TODO Make them configurable.
      var lowLimit = 350;
      var highLimit = 550;

      var widgetWidth = container.width();
      var items = container.find('li');
      var images = container.find('img');
      var linkParagraphs = items.find('p.link');

      // TODO Move styles to CSS and only trigger the class.
      if (widgetWidth <= lowLimit) {
        items.css({
          "width": "100%",
          "min-height": "90px"
        });
        images.css("width", "100%");
        linkParagraphs.css({
          "width": "100%",
          "max-width": "100%",
          "margin-left": "0"
        });
      }
      else if (widgetWidth > lowLimit && widgetWidth <= highLimit) {
        items.css({
          "width": "100%",
          "min-height": "90px"
        });
        images.css("width", "38%");
        linkParagraphs.css({
          "width": "59%",
          "max-width": "100%",
          "margin-left": "2%"
        });
      }
      else {
        items.css({
          "width": "32.3%",
          "min-height": "0"
        });
        images.css("width", "100%");
        linkParagraphs.css({
          "width": "100%",
          "max-width": "100%",
          "margin-left": "0"
        });
      }
    }

  });

  /**
   * Sidebar widget class.
   *
   * @class
   * @extends Contextly.TabsWidget
   */
  Contextly.SidebarWidget = Contextly.createClass( /** @lends Contextly.SidebarWidget.prototype */ {

    /**
     * Parent class.
     */
    extend: Contextly.TabsWidget,

    /**
     * Returns the widget type.
     *
     * @returns {string}
     */
    getWidgetType: function () {
      return Contextly.WidgetType.SIDEBAR;
    },

    /**
     * @inheritDoc
     */
    buildWidget: function () {
      var value,
        html = '';
      html += '<div class="linker_content">';

      // Add sidebar title and description if they are not empty.
      if (value = this.widget.name) {
        html += '<div class="title">' + Drupal.checkPlain(value) + '</div>';
      }
      if (value = this.widget.description) {
        // TODO: Description is a multi-line area, we should probably convert
        // newlines to <br>.
        html += '<div class="description">' + Drupal.checkPlain(value) + '</div>';
      }

      // Add the list of links.
      html += '<ul class="link' + ( this.sectionHasImages('previous') ? ' linker_images' : '' ) + '">';
      html += this.buildSectionLinks('previous');
      html += '</ul>';

      // Close the wrapper.
      html += '</div>';

      return html;
    },

    /**
     * @inheritDoc
     */
    buildSectionLinks: function (section) {
      var html = "";
      if (this.widget.links && this.widget.links[section]) {
        for (var index in this.widget.links[section]) {
          var link = this.widget.links[section][index];

          if (link.id && link.title) {
            html += "<li>" + this.buildLink(link) + "</li>";
          }
        }
      }

      return html;
    },

    /**
     * @inheritDoc
     */
    getCSSTemplate: function () {
      return this.widgetStylesURL + this.getWidgetType() + '/template-' + this.settings.theme + ".css";
    },

    /**
     * @inheritDoc
     */
    buildSettingsCSS: function(prefix) {
      var s = this.settings;

      var code = "";

      if (s.css_code) {
        // TODO Escape it properly.
        code += this.escape(s.css_code);
      }

      if (s.font_family) {
        code += this.buildCSSRule(prefix, "a.title", "font-family", s.font_family);
      }
      if (s.font_size) {
        code += this.buildCSSRule(prefix, "a.title", "font-size", s.font_size);
      }

      if (s.color_background) {
        code += this.buildCSSRule(prefix, ".linker_content", "background-color", s.color_background);
        code += this.buildCSSRule(prefix, ".linker_images img", "border-color", s.color_background);
      }

      if (s.color_links) {
        code += this.buildCSSRule(prefix, ".linker_content a", "color", s.color_links);
        code += this.buildCSSRule(prefix, ".linker_content span", "color", s.color_links);
      }

      if (s.color_border) {
        code += this.buildCSSRule(prefix, ".linker_content", "border-color", s.color_border);
        code += this.buildCSSRule(prefix, ".linker_tabs li.active span", "border-color", s.color_border);
        code += this.buildCSSRule(prefix, ".linker_tabs span", "border-color", s.color_border);
      }

      return code;
    },

    /**
     * @inheritDoc
     */
    attachEventHandlers: function (container) {
      // Call BaseWidget function to attach basic handlers only.
      Contextly.BaseWidget.prototype.attachEventHandlers.call(this, container);
    }

  });

  /**
   * Helper object to load CSS files and inline code once.
   */
  Contextly.CSSLoader = {

    /**
     * The list of already loaded external CSS URLs.
     */
    loadedURLs: {},

    /**
     * Initiates loading of external CSS file with passed in URL.
     *
     * @param {string} url
     */
    loadOnce: function (url) {
      if (!url) {
        return;
      }

      // Make sure we're not going to load the same CSS file again.
      if (this.loadedURLs[url]) {
        return;
      }

      // Mark URL as loaded.
      this.loadedURLs[url] = true;

      // Load it.
      $('<link>')
        .attr({
          rel: "stylesheet",
          media: "screen",
          type: "text/css",
          href: url
        })
        .appendTo('head');
    },

    /**
     * The list of widget classes with already applied settings CSS.
     */
    processedClasses: {},

    areSettingsApplied: function(widgetClass) {
      return this.processedClasses[widgetClass];
    },

    /**
     * Adds CSS rules of the widget settings to the head.
     *
     * For each widget class this is done only once.
     *
     * @param widgetClass
     *   Class that identifies the widget.
     * @param css
     *   CSS for the settings build by the widget itself.
     */
    applySettingsOnce: function(widgetClass, css) {
      // Make sure we're not going to build the same settings again.
      if (this.areSettingsApplied(widgetClass)) {
        return;
      }

      // Mark class as already processed.
      this.processedClasses[widgetClass] = true;

      // Inject CSS to the page head.
      if (css) {
        $('<style type="text/css" media="screen">' + css + "</style>")
          .appendTo('head');
      }
    }

  };

  Contextly.MainServerAjaxClient = {
    init: function(settings) {
      this.remoteURL = settings.mainProviderURL;
    },

    getXHR: function() {
      if (!this.xhr) {
        this.xhr = new easyXDM.Rpc({
          remote: this.remoteURL
        }, {
          remote: {
            request: {}
          }
        });
      }

      return this.xhr;
    },

    call: function (url, callback) {
      this.getXHR().request({
        url: url,
        method: "POST"
      }, function (response) {
        if (callback) {
          callback(response);
        }
      });
    }

  };

  /**
   * Fires Contextly page event.
   *
   * @param {string} name
   *   Event name.
   * @param {string} [key]
   *   Event parameter value.
   */
  Contextly.firePageEvent = function(widgetId, name, key) {
    $(window).triggerHandler('contextlyPageEvent', {
      widgetId: widgetId,
      name: name,
      key: typeof key !== 'undefined' ? key : null,
      date: new Date()
    });
  };

  /**
   * Loads settings of all page snippets on the node page and displays them.
   *
   * Also, registers the page events tracking handler which submits the list
   * of events to the Contextly.
   */
  Drupal.behaviors.contextlyWidgets = {
    attach: function (context, settings) {
      if (!settings.contextlyWidgets) {
        return;
      }

      // Make sure we initialize widgets only once on the page.
      var isFirstTime = $('body', context)
        .once('contextly-widgets')
        .size();
      if (!isFirstTime) {
        return;
      }

      var s = $.extend({
        nid: 0,
        version: '',
        mode: '',
        sitePath: '',
        apiProviderURL: null,
        mainProviderURL: null,
        settingsURL: null,
        pageEventsURL: null
      }, settings.contextlyWidgets);

      if (!s.nid || !s.version || !s.sitePath || !s.apiProviderURL || !s.mainProviderURL || !s.settingsURL) {
        return;
      }

      // Get widget containers and make sure they're not empty.
      var containers = $('.contextly-placeholder', context);
      if (!containers.size()) {
        return;
      }

      /**
       * Contextly events listener.
       *
       * Submits event data to the Contextly service.
       *
       * @param e
       * @param data
       */
      var onPageEvent = function(e, data) {
        var event = {
          post_id: s.nid,
          setting_id: data.widgetId,
          event_name: data.name,
          event_key: data.key,
          event_date: data.date
        };
        restCall(s.pageEventsURL, event);
      };

      var attachPageEventHandlers = function() {
        $(window).bind('contextlyPageEvent', onPageEvent);
      };

      var onSettingsLoadingSuccess = function(data) {
        // Remove loading class.
        containers
          .removeClass('contextly-loading contextly-placeholder')
          .addClass('contextly-widget');

        // Attach page event handlers.
        if (s.pageEventsURL) {
          attachPageEventHandlers();
        }

        // Init main server AJAX client.
        Contextly.MainServerAjaxClient.init(s);

        // Render snippets.
        $.each(data.entry.snippets, function() {
          // Provide defaults for the automatically generated snippet.
          var widgetData = $.extend({
            id: 'default',
            type: 'normal'
          }, this);
          var container = containers
            .filter('.contextly-snippet-placeholder:first')
            .removeClass('contextly-snippet-placeholder')
            .addClass('contextly-snippet contextly-widget--' + widgetData.id);
          var widget = Contextly.WidgetFactory.getWidget(widgetData, s);
          if (widget) {
            widget.display(container);

            // Remove processed container from the list of pending ones.
            containers = containers.not(container);
          }

          // For now we support only one snippet.
          return false;
        });

        // Render sidebars.
        $.each(data.entry.sidebars, function() {
          var widgetData = this;
          var selector = '.contextly-widget--' + widgetData.id;
          var sidebarContainers = containers
            .filter(selector)
            .removeClass('contextly-sidebar-placeholder')
            .addClass('contextly-sidebar contextly-sidebar-' + widgetData.layout);
          sidebarContainers.each(function() {
            var container = $(this);
            var widget = Contextly.WidgetFactory.getWidget(widgetData, s);
            if (widget) {
              widget.display(container);

              // Remove processed containers from the list of pending ones.
              containers = containers.not(container);
            }
          });
        });

        // Remove placeholders left unprocessed.
        containers.remove();
      };

      var onSettingsLoadingFailure = function() {
        containers
          .removeClass('contextly-loading')
          .addClass('contextly-error');
      };

      var loadSettings = function() {
        containers.addClass('contextly-loading');
        restCall(s.settingsURL, {
          page_id: s.nid,
          admin: 0
        }, function (response) {
          if (response.data) {
            var data = easyXDM.getJSONObject().parse(response.data);
            if (data.success && data.entry) {
              onSettingsLoadingSuccess(data);
              return;
            }
          }

          // There was no data if we got here.
          onSettingsLoadingFailure();
        });
      };

      var rpc = null;
      var initRpc = function () {
        rpc = new easyXDM.Rpc({
          remote: s.apiProviderURL
        },
        {
          remote: {
            request: {}
          }
        });
      };

      var restCall = function(url, params, callback) {
        if (!rpc) {
          // Create RPC proxy only once per page.
          initRpc();
        }

        // Make sure we pass at least blank function as a callback.
        if (typeof callback !== 'function') {
          callback = function() {};
        }

        // Add common parameters.
        $.extend(params, {
          site_path: s.sitePath,
          version: s.version
        });

        // Make the request.
        rpc.request({
          url: url,
          method: "POST",
          data: params
        }, callback);
      };

      loadSettings();
    }
  };

})(jQuery);
