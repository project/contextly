(function($) {

  Drupal.behaviors.contextlyNodeForm = {
    attach: function(context, settings) {
      $('fieldset#edit-contextly', context)
        .once('contextly-drupal-summary')
        .drupalSetSummary(function(context) {
          var vals = [];

          var disabled = $('input[name="contextly_disabled"]', context).is(':checked');
          if (disabled) {
            vals.push(Drupal.t('Disabled'));
          }
          else {
            vals.push(Drupal.t('Enabled'));
          }

          return vals.join(', ');
        });
    }
  };

})(jQuery);
