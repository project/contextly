(function ($) {

  Contextly = window.Contextly || {};

  Contextly.overlay = {
    options: {
      zIndex: 1000,
      overlayColor: 'black',
      overlayOpacity: 0.5,
      dialogColor: 'white',
      dialogPadding: 20,
      width: null,
      height: null,
      spacing: 40,
      dataKey: 'contextlyOverlay',
      onCloseButton: null
    },

    overlay: null,
    dialog: null,
    inner: null,
    closeButton: null,
    content: null,

    _init: function() {
      if (this.overlay === null) {
        this.overlay = $('<div class="contextly-overlay" />')
          .hide()
          .appendTo('body');
      }
      this.overlay.css({
        zIndex: this.options.zIndex,
        backgroundColor: this.options.overlayColor,
        opacity: this.options.overlayOpacity
      });

      if (this.dialog === null) {
        this.dialog = $('<div class="contextly-overlay-dialog" />')
          .hide()
          .appendTo('body');
      }
      this.dialog.css({
        backgroundColor: this.options.dialogColor,
        zIndex: this.options.zIndex + 1
      });

      if (this.inner === null) {
        this.inner = $('<div class="contextly-overlay-content" />')
          .appendTo(this.dialog);
      }
      this.inner.css({
        zIndex: this.options.zIndex + 2
      });

      if (this.closeButton === null) {
        this.closeButton = $('<a href="javascript:" class="contextly-overlay-close"></a>')
          .bind('click', this._proxy(this._onCloseButton))
          .appendTo(this.dialog);
      }
      this.closeButton.css({
        zIndex: this.options.zIndex + 3
      });
    },

    open: function(element, options) {
      // Options & elements init.
      $.extend(this.options, options);
      this._init();
      this.content = element;

      // Set up event handlers.
      $(window)
        .bind('resize.contextlyOverlay', this._proxy(this._onWindowResize));

      // Notify world.
      this._fireEvent('contextlyOverlayBeforeOpen');

      // Lock the scroll first.
      this._lockBodyScroll();

      // Display overlay & dialog.
      this.overlay.show();
      this.inner.append(element);
      this.dialog.show();

      this._refreshDialogSize();

      // Notify world again.
      this._fireEvent('contextlyOverlayAfterOpen');
    },

    close: function() {
      // Notify world.
      this._fireEvent('contextlyOverlayBeforeClose');

      this.content.remove();
      this.content = null;

      this.dialog.hide();
      this.overlay.hide();
      this._unlockBodyScroll();

      $(window).unbind('.contextlyOverlay');

      // Notify world again.
      this._fireEvent('contextlyOverlayAfterClose');
    },

    setCloseButtonHandler: function(callback) {
      this.options.onCloseButton = callback;
    },

    _onCloseButton: function() {
      // Don't close the overlay in case
      if ($.isFunction(this.options.onCloseButton) && this.options.onCloseButton() === false) {
        return;
      }

      this.close();
    },

    _onWindowResize: function() {
      this._refreshDialogSize();
    },

    _refreshDialogSize: function() {
      var wnd = $(window);

      var dimensions = {
        'width': 'left',
        'height': 'top'
      };
      for (var side in dimensions) {
        var position = dimensions[side];
        var windowSize = wnd[side]();
        var dialogSize = 0;

        // Set dialog width/height first.
        if (typeof this.options[side] === 'number') {
          // Fixed size.
          dialogSize = this.options[side];
        }
        else {
          // Size depends on the window size.
          dialogSize = windowSize - this.options.spacing * 2;

          if (this.options[side] && this.options[side].max) {
            // There is a top limit for the dimension, make sure we're not
            // exceeding it.
            if (dialogSize > this.options[side].max) {
              dialogSize = this.options[side].max;
            }
          }
        }
        this.dialog.css(side, dialogSize);
        this.inner.css(side, dialogSize - 2 * this.options.dialogPadding);

        // Set top/left depending on the height/width of window and dialog.
        var dialogPosition = Math.floor((windowSize - dialogSize) / 2);
        this.dialog.css(position, dialogPosition);
        this.inner.css(position, dialogPosition + this.options.dialogPadding);
      }
    },

    _proxy: function(func) {
      var self = this;
      return function() {
        func.apply(self, arguments);
      }
    },

    _fireEvent: function (type) {
      // Remove the type of event.
      var args = Array.prototype.slice.call(arguments, 1);

      $(window).triggerHandler(type, args);
    },

    _lockBodyScroll: function() {
      var $window = $(window);
      var $document = $(document);
      var $html = $('html');

      var existingStyle = $html.attr('style');
      var data = {
        scroll: {
          top: $window.scrollTop(),
          left: $window.scrollLeft()
        },
        style: existingStyle ? existingStyle : ''
      };

      // The trick here is that the fixed element with dimensions larger than
      // the window doesn't trigger scrollbars.
      var css = {
        position: 'fixed',
        top: -data.scroll.top,
        left: -data.scroll.left,
        width: '100%'
      };

      // To avoid page "jumping" we force empty scrollbars in case they were
      // visible because of the document height.
      if ($document.height() > $window.height()) {
        css['overflow-y'] = 'scroll';
      }

      $html
        .data(this.options.dataKey, data)
        .css(css);
    },

    _unlockBodyScroll: function() {
      var $html = $('html');
      var data = $html.data(this.options.dataKey);
      if (data) {
        // Restore "style" attribute back to its state before we locked the
        // document scrolling. This also removes the scroll lock and our styles.
        $html.attr('style', data.style);

        // Scroll the page back to its original position.
        $(window)
          .scrollTop(data.scroll.top)
          .scrollLeft(data.scroll.left);
      }
    }
  };

})(jQuery);
