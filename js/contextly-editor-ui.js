(function ($) {

  // Init global Contextly namespace if not already done.
  Contextly = window.Contextly || {};
  Contextly.overlayDialog = {};

  /**
   * Base overlay class.
   *
   * @class
   */
  Contextly.overlayDialog.Base = Contextly.createClass(/** @lends Contextly.overlayDialog.Base.prototype */ {

    construct: function(content) {
      this.e = {};
      this.content = content;
      this.templates = {};
      this.settings = api.getSettings();
      this.initState();

      this.initHidden();
      this.extractTemplates();
      this.findElements();
      this.attachEventHandlers();
      this.initUI();
    },

    initState: function() {
      this.state = {
        inProgress: false,
        lockedControls: null,
        previewUrl : null,
        scrollTop: 0,
        scrollLeft: 0
      };

      // By default support only links search.
      this.searchTypes = {
        links: {
          search: this.proxy(this.performLinksSearchQuery),
          render: this.proxy(this.renderLinksSearchResults)
        }
      };
    },

    extractTemplates: function() {
      var templates = {};

      var templateElements;
      var selector = '*[data-template]:not(:has(*[data-template]))';
      while ((templateElements = this.content.find(selector)) && templateElements.size() > 0) {
        templateElements.each(function() {
          var element = $(this);

          var template = {
            element: element,
            name: element.attr('data-template')
          };

          // Detach element from the DOM and cleanup it from special attributes
          // to just clone later.
          element
            .detach()
            .removeAttr('data-template');

          templates[template.name] = template;
        });
      }

      this.templates = templates;
    },

    getTemplate: function(name) {
      return this.templates[name].element.clone(true);
    },

    showProgressIndicator: function() {
      this.e.progressIndicator.show();
      this.state.inProgress = true;

      this.state.lockedControls = this.content.find('button');
      this.lockControls(this.state.lockedControls, 'global');
    },

    hideProgressIndicator: function() {
      this.e.progressIndicator.hide();
      this.state.inProgress = false;

      if (this.state.lockedControls) {
        this.unlockControls(this.state.lockedControls, 'global');
        this.state.lockedControls = null;
      }
    },

    lockControls: function(controls, reason) {
      // Mark each control with the reason of locking. This way we can lock the
      // same controls for multiple reasons at the same time and unlock them
      // only if all reasons are gone.
      var dataKey = 'contextlyLocked';
      for (var i = 0; i < controls.length; i++) {
        var control = controls.eq(i);

        var reasons = control.data(dataKey);
        if (!reasons) {
          reasons = {};
          control.data(dataKey, reasons);
        }
        reasons[reason] = true;
      }

      // Lock all at once.
      controls.attr('disabled', 'disabled');
    },

    unlockControls: function(controls, reason) {
      for (var i = 0; i < controls.length; i++) {
        var control = controls.eq(i);

        // Remove passed reason from the list.
        var reasons = control.data('contextlyLocked');
        if (reasons) {
          delete reasons[reason];
        }

        if ($.isEmptyObject(reasons)) {
          control.removeData('contextlyLocked');
          control.removeAttr('disabled');
        }
      }
    },

    initHidden: function() {
      $('.hidden')
        .removeClass('hidden')
        .hide();
    },

    initUI: function() {
      this.initSearchTabs();
    },

    findElements: function() {
      // Alert.
      this.e.alert = this.content.find('.alert');
      this.e.alertText = this.e.alert.find('.alert-text');
      this.e.alertClose = this.e.alert.find('.close');

      // Search controls.
      this.e.searchInput = this.content.find('.search-phrase');
      this.e.searchSubmit = this.content.find('.search-submit');

      // URL Controls.
      this.e.urlInput = this.content.find('.link-url');
      this.e.urlSubmit = this.content.find('.url-submit');

      // Search tabs, results and pager.
      this.e.searchTabs = this.content.find('.search-tabs');
      this.e.searchNoResults = this.content.find('.search-no-results');
      this.e.searchResults = this.content.find('.search-results');

      // Search pager.
      this.e.searchPager = this.content.find('.search-pager');
      this.e.searchNext = this.e.searchPager.find('.search-next');
      this.e.searchPrev = this.e.searchPager.find('.search-prev');

      // Progress indicator.
      this.e.progressIndicator = $('.progress-indicator');

      // Inline URL preview.
      this.e.urlPreview = $('.url-preview-overlay');
      this.e.urlPreviewFrame = this.e.urlPreview.find('.url-preview-frame');
      this.e.urlPreviewLink = this.e.urlPreview.find('.url-preview-link');
      var urlPreviewButtons = this.e.urlPreview.find('.url-preview-buttons');
      this.e.urlPreviewRemove = urlPreviewButtons.find('.url-preview-remove');
      this.e.urlPreviewCancel = urlPreviewButtons.find('.url-preview-cancel');
      this.e.urlPreviewConfirm = urlPreviewButtons.find('.url-preview-confirm');
    },

    attachEventHandlers: function() {
      // Alert close handler. We don't use Bootstrap plugin here, because it
      // completely removes the alert and prevents it from re-appearing.
      this.bindClick(this.e.alertClose, this.onAlertClose);

      // Search submit handlers.
      this.bindEnter(this.e.searchInput, this.onSearchSubmit);
      this.bindClick(this.e.searchSubmit, this.onSearchSubmit);

      // URL submit handlers.
      this.bindEnter(this.e.urlInput, this.onUrlSubmit);
      this.bindClick(this.e.urlSubmit, this.onUrlSubmit);

      // Search tabs events.
      var searchTabLink = this.templates.searchTab.element
        .find('.search-tab-link');
      this.bindClick(searchTabLink, this.onSearchTabSwitch, true);

      // Pager events.
      this.bindClick(this.e.searchNext, this.onSearchNext);
      this.bindClick(this.e.searchPrev, this.onSearchPrev);

      // Search result.
      var searchResult = this.templates.searchLinksResult.element;
      var addButton = searchResult.find('.website-add');
      this.bindClick(addButton, this.onSearchResultAdd, true);
      var testLink = searchResult.find('.website-link');
      this.bindClick(testLink, this.onSearchResultPreview, true);

      // Inline URL preview.
      this.bindClick(this.e.urlPreviewLink, this.onLinkNewTabPreview, true);
      this.bindClick(this.e.urlPreviewCancel, this.closeUrlPreview);
    },

    /**
     * Binds namespaced event handlers that are called on this object context.
     *
     * @param element
     * @param event
     *   Event name.
     * @param func
     *   Function that will be called with "this" as a context.
     * @param [passArguments]
     *   Pass TRUE to call func with arguments of original callback prepended
     *   with the context of original function.
     */
    bind: function(element, event, func, passArguments) {
      var self = this;
      element.bind(event, function() {
        if (self.state.inProgress) {
          return false;
        }

        if (passArguments) {
          Array.prototype.unshift.call(arguments, this);
          return func.apply(self, arguments);
        }
        else {
          return func.call(self);
        }
      });
    },

    /**
     * Binds click event handler that is called on this object context.
     *
     * @param element
     * @param func
     *   Function that will be called with "this" as a context.
     * @param [passArguments]
     *   Pass TRUE to call func with arguments of original callback prepended
     *   with the context of original function.
     */
    bindClick: function(element, func, passArguments) {
      var self = this;
      element.bind('click', function(e) {
        if (self.state.inProgress) {
          return false;
        }

        // React on left mouse button only. IE 8 doesn't provide this info and
        // e.which is 0. See http://bugs.jquery.com/ticket/13209
        if (!e.which || e.which == 1) {
          if (passArguments) {
            Array.prototype.unshift.call(arguments, this);
            return func.apply(self, arguments);
          }
          else {
            return func.call(self);
          }
        }
      });
    },

    /**
     * Helper to bind Enter key press to the input field.
     *
     * @param input
     * @param func
     * @param [passArguments]
     *   Pass TRUE to call func with arguments of original callback prepended
     *   with the context of original function.
     */
    bindEnter: function(input, func, passArguments) {
      var self = this;
      input.bind('keypress', function(e) {
        if (self.state.inProgress) {
          return false;
        }

        if (e.which == 13) {
          if (passArguments) {
            Array.prototype.unshift.call(arguments, this);
            return func.apply(self, arguments);
          }
          else {
            return func.call(self);
          }
        }
      });
    },

    /**
     * Displays alert on top of the dialog.
     *
     * @param text
     * @param [type]
     *   One of the following:
     *   - error (default)
     *   - success
     *   - warning
     *   - info
     */
    displayAlert: function (text, type) {
      type = type || 'error';
      switch (type) {
        case 'warning':
          break;

        default:
          this.e.alert
            .attr('data-alert-type', type)
            .addClass('alert-' + type);
          break;
      }

      this.e.alertText.text(text);
      this.e.alert.show();
    },

    onAlertClose: function () {
      this.e.alert.hide();
      this.e.alertText.html('');

      var alertType = this.e.alert.attr('data-alert-type');
      if (alertType) {
        this.e.alert
          .removeClass('alert-' + alertType)
          .removeAttr('data-alert-type');
      }
    },

    onDialogEsc: function() {
      api.closeOverlay();
    },

    performAjaxRequest: function (method, params, xhrData) {
      params = $.extend({
        url: api.buildAjaxUrl(method, this.settings.nid),
        type: 'POST',
        dataType: 'json',
        context: this
      }, params);
      var jqXHR = $.ajax(params);

      // Fill in additional data that is passed to all callbacks in properties
      // of the jqXHR object.
      if (!xhrData) {
        xhrData = params.data;
      }
      jqXHR.contextlyData = xhrData;
      jqXHR.contextlyMethod = method;
    },

    findActiveSearchTab: function () {
      var tabs = this.e.searchTabs
        .find('.search-tab');
      var active = tabs.filter('.active');
      if (active.size()) {
        return active;
      }
      else {
        // Fallback to the first links search tab if there is no active tab.
        return tabs
          .filter('[data-search-type="links"]')
          .first();
      }
    },

    initSearchTabs: function () {
      for (var i = 0; i < this.settings.annotations.length; i++) {
        var annotation = this.settings.annotations[i];

        this.getTemplate('searchTab')
          .attr('data-site-url', annotation.site_url)
          .find('.search-tab-link')
          .text(annotation.site_name)
          .end()
          .appendTo(this.e.searchTabs);
      }
    },

    proxy: function(callback, context) {
      if (!context) {
        context = this;
      }

      return $.proxy(callback, context);
    },

    createTabSearchInfo: function(tab) {
      var type = tab.attr('data-search-type');
      var siteUrl = tab.attr('data-site-url');
      return new Contextly.overlayDialog.SearchInfo(type, siteUrl);
    },

    getTabSearchInfo: function(tab) {
      var dataKey = 'contextlySearchInfo';
      var searchInfo = tab.data(dataKey);
      if (!searchInfo) {
        searchInfo = this.createTabSearchInfo(tab);
        tab.data(dataKey, searchInfo);
      }
      return searchInfo;
    },

    onSearchNext: function () {
      var searchInfo = this.getTabSearchInfo(this.findActiveSearchTab());
      var cache = this.state.searchResults;
      var cacheKey = searchInfo.toStr();
      if (this.state.searchQuery && cache[cacheKey] && cache[cacheKey].nextPage) {
        var nextPage = parseInt(cache[cacheKey].page) + 1;
        this.performSearchQuery(searchInfo, this.state.searchQuery, nextPage);
      }

      return false;
    },

    onSearchPrev: function () {
      var searchInfo = this.getTabSearchInfo(this.findActiveSearchTab());
      var cache = this.state.searchResults;
      var cacheKey = searchInfo.toStr();
      if (this.state.searchQuery && cache[cacheKey]) {
        var currentPage = parseInt(cache[cacheKey].page);
        if (currentPage > 1) {
          this.performSearchQuery(searchInfo, this.state.searchQuery, currentPage - 1);
        }
      }

      return false;
    },

    onSearchSubmit: function () {
      var query = this.e.searchInput.val();
      if (!query) {
        // No search terms. Do nothing.
        // TODO Mark input with error class and display error message.
        return;
      }

      var searchInfo = this.getTabSearchInfo(this.findActiveSearchTab());
      this.performSearchQuery(searchInfo, query, 1);
    },

    onSearchTabSwitch: function (target) {
      var tab = $(target).closest('.search-tab');

      if (!tab.is('.active') && this.state.searchQuery) {
        var searchInfo = this.getTabSearchInfo(tab);
        var cacheKey = searchInfo.toStr();
        if (this.state.searchResults[cacheKey]) {
          // Get results from cache and display immediately.
          this.activateTab(searchInfo);

          var cacheEntry = this.state.searchResults[cacheKey];
          this.displaySearchResults(cacheEntry.results);
          this.displayPager(cacheEntry.page, cacheEntry.nextPage);
        }
        else {
          // No cache entry, perform search query.
          this.performSearchQuery(searchInfo, this.state.searchQuery, 1);
        }
      }

      return false;
    },

    performSearchQuery: function(searchInfo, query, page) {
      this.checkSearchType(searchInfo);
      this.showProgressIndicator();
      this.searchTypes[searchInfo.type].search(searchInfo, query, page);
    },

    performLinksSearchQuery: function(searchInfo, query, page) {
      this.performAjaxRequest('search', {
        data: {
          type: searchInfo.type,
          query: query,
          page: page,
          site_url: searchInfo.siteUrl
        },
        success: this.onSearchRequestSuccess,
        error: this.onSearchRequestError,
        complete: this.hideProgressIndicator
      });
    },

    escapeSizzleAttrValue: function(value) {
      if (!value) {
        return '';
      }

      return value.replace('"', '\\\"');
    },

    activateTab: function (searchInfo) {
      var tabs = this.e.searchTabs
        .find('.search-tab');
      tabs
        .filter('.active')
        .removeClass('active');

      // Escape type and URL.
      var escapedType = this.escapeSizzleAttrValue(searchInfo.type);
      var escapedUrl = this.escapeSizzleAttrValue(searchInfo.siteUrl);

      tabs
        .filter('[data-search-type="' + escapedType + '"][data-site-url="' + escapedUrl + '"]:first')
        .addClass('active');
    },

    onSearchRequestSuccess: function (data) {
      if (data.query !== this.state.searchQuery) {
        // Reset search results cache if search is performed on another query.
        this.state.searchResults = {};
        this.state.searchQuery = data.query;
      }

      // Store rendered results in cache for fast tab switching.
      var searchInfo = new Contextly.overlayDialog.SearchInfo(data.type, data.siteUrl);
      var cacheEntry = {
        page: data.page,
        nextPage: data.nextPage,
        results: this.renderSearchResults(searchInfo, data.list ? data.list : [])
      };
      this.state.searchResults[searchInfo.toStr()] = cacheEntry;

      // Activate tab, display results and pager.
      this.activateTab(searchInfo);
      this.displaySearchResults(cacheEntry.results);
      this.displayPager(cacheEntry.page, cacheEntry.nextPage);

      // Scroll to top of the page in case user loaded results using pager.
      this.scrollToTopLeft();
    },

    scrollToTopLeft: function() {
      var $window = $(window);
      var scrollTop = $window.scrollTop();
      var scrollLeft = $window.scrollLeft();
      if (scrollTop <= 0 && scrollLeft <= 0) {
        return;
      }

      $window.scrollTo(0, 300);
    },

    onSearchRequestError: function () {
      this.displayAlert('Unable to perform search request. Something went wrong.');
    },

    displaySearchNoResults: function () {
      this.e.searchTabs.show();
      this.e.searchPager.hide();
      this.e.searchNoResults.show();
      this.e.searchResults.hide();
    },

    displaySearchResults: function (results) {
      if (!results.length) {
        this.displaySearchNoResults();
        return;
      }

      this.e.searchTabs.show();
      this.e.searchNoResults.hide();

      // Iterate over rendered search results and put them to the result set.
      this.e.searchResults
        .children()
        .detach();
      for (var i = 0; i < results.length; i++) {
        this.e.searchResults.append(results[i]);
      }

      // Finally display search results.
      this.e.searchResults.show();
    },

    displayPager: function (page, nextPage) {
      var prevWrapper = this.e.searchPrev.closest('.previous');
      if (parseInt(page) <= 1) {
        prevWrapper
          .not('.disabled')
          .addClass('disabled');
      }
      else {
        prevWrapper
          .filter('.disabled')
          .removeClass('disabled');
      }

      var nextWrapper = this.e.searchNext.closest('.next');
      if (nextPage) {
        nextWrapper
          .filter('.disabled')
          .removeClass('disabled');
      }
      else {
        nextWrapper
          .not('.disabled')
          .addClass('disabled');
      }

      this.e.searchPager.show();
    },

    checkSearchType: function(searchInfo) {
      if (!this.searchTypes[searchInfo.type]) {
        $.error('Unknown search type "' + searchInfo.type + '" passed.');
      }
    },

    renderSearchResults: function (searchInfo, links) {
      this.checkSearchType(searchInfo);
      return this.searchTypes[searchInfo.type].render(links);
    },

    renderLinksSearchResults: function (links) {
      var results = [];
      if (links && links.length) {
        for (var i = 0; i < links.length; i++) {
          var link = links[i];
          var searchResult = this.getTemplate('searchLinksResult')
            .attr('data-url', link.url);

          // Fill title, description and the link.
          searchResult
            .find('.search-result-title')
            .text(link.title);
          searchResult
            .find('.search-result-description')
            .text(link.description);
          searchResult
            .find('.website-link')
            .attr('href', link.url)
            .find('.website-url')
            .text(link.display_url);

          results.push(searchResult);
        }
      }
      return results;
    },

    onUrlSubmit: function () {
      var url = this.e.urlInput.val();
      if (url) {
        this.performUrlInfoRequest(url);
      }
      else {
        // TODO Mark input with error class.
      }

      return false;
    },

    onSearchResultAdd: function (target) {
      var url = $(target)
        .closest('.search-result')
        .attr('data-url');
      this.addSearchResultUrl(url);
    },

    addSearchResultUrl: function(url) {
      this.performUrlInfoRequest(url);
    },

    performUrlInfoRequest: function(url) {
      if (!url) {
        return false;
      }

      this.performAjaxRequest('url-info', {
        data: {
          url: url
        },
        success: this.onUrlInfoRequestSuccess,
        error: this.onUrlInfoRequestError,
        complete: this.onUrlInfoRequestComplete
      });
      return true;
    },

    onUrlInfoRequestSuccess: Contextly.abstractMethod(),

    onUrlInfoRequestError: Contextly.abstractMethod(),

    onUrlInfoRequestComplete: function() {
      // Do nothing by default.
    },

    onSearchResultPreview: function(target, e) {
      e.preventDefault();

      // Set up buttons state and attach event handlers.
      this.e.urlPreviewRemove.hide();
      this.e.urlPreviewConfirm
        .unbind('click')
        .show();
      this.bindClick(this.e.urlPreviewConfirm, this.onSearchResultPreviewConfirm);

      // Show the preview.
      this.previewUrl($(target).attr('href'));
    },

    onSearchResultPreviewConfirm: function() {
      // Get URL now, because it will be reset by closeUrlPreview().
      var url = this.state.previewUrl;

      this.closeUrlPreview();
      this.performUrlInfoRequest(url);
    },

    previewUrl: function(url) {
      // Save url for future use.
      this.state.previewUrl = url;

      // Lock main scroll.
      this.lockEditorScroll();

      // Set up and show preview.
      api.setOverlayCloseButtonHandler(this.proxy(this.onPreviewOverlayCloseButton));
      this.e.urlPreviewFrame.attr('src', url);
      this.e.urlPreviewLink.attr('href', url);
      this.e.urlPreview.show();
    },

    lockEditorScroll: function() {
      var $window = $(window);

      // Save current position to restore later.
      this.state.scrollTop = $window.scrollTop();
      this.state.scrollLeft = $window.scrollLeft();

      // The trick here is that the fixed element with dimensions larger than
      // the window doesn't trigger scrollbars.
      $('html').css({
        position: 'fixed',
        top: -this.state.scrollTop,
        left: -this.state.scrollLeft,
        width: '100%'
      });
    },

    unlockEditorScroll: function() {
      $('html').css({
        position: 'static',
        top: 'auto',
        left: 'auto',
        width: 'auto'
      });

      $(window)
        .scrollTop(this.state.scrollTop)
        .scrollLeft(this.state.scrollLeft);
    },

    onPreviewOverlayCloseButton: function() {
      this.closeUrlPreview();

      // Prevent overlay closing when preview is active.
      return false;
    },

    closeUrlPreview: function() {
      // Cleanup saved URL.
      this.state.previewUrl = null;

      // Cleanup and hide preview overlay.
      api.setOverlayCloseButtonHandler(null);
      this.e.urlPreviewFrame.attr('src', 'about:blank');
      this.e.urlPreview.hide();

      // Restore scroll.
      this.unlockEditorScroll();
    },

    onLinkNewTabPreview: function(target, e) {
      e.preventDefault();
      window.parent.open($(target).attr('href'));
    },

    onTextareaEnter: function (target, e) {
      e.preventDefault();
    },

    normalizeSpace: function(text) {
      // This also replaces newlines with space.
      return text
        .replace(/\s+/, ' ')
        .replace(/^\s|\s$/, '');
    },

    indexAnnotations: function() {
      this.relatedHosts = {};

      // Parse current URL too, even if it's not added to the list of
      // annotations.
      var hostname = this.extractUrlHostname(this.settings.baseUrl);
      this.relatedHosts[hostname] = true;

      for (var i = 0; i < this.settings.annotations.length; i++) {
        var annotation = this.settings.annotations[i];
        if (annotation.site_url) {
          hostname = this.extractUrlHostname(annotation.site_url);
          this.relatedHosts[hostname] = true;
        }
      }
    },

    extractUrlHostname: function (url) {
      url = this.parseUrl(url);
      return url.hostname
        .toLowerCase()
        .replace(/^\.+|\.+$/, '')
        .replace(/^w{3}\./, '');
    },

    parseUrl: function (url) {
      // We create a link using passed URL to use its "location"-like properties
      // later as a parsing result.
      var link = document.createElement("a");
      link.href = url;

      // IE doesn't populate all link properties when setting .href with a relative URL,
      // however .href will return an absolute URL which then can be used on itself
      // to populate these additional fields.
      if (link.host == "") {
        link.href = link.href;
      }

      return link;
    },

    /**
     * Returns section name for the passed link.
     *
     * This function depends on indexed host names for search tabs.
     */
    chooseUrlSection: function (url) {
      var urlHostname = this.extractUrlHostname(url);

      // We split URL domain name on dots and try to find a match with search
      // tabs domains iterating from full URL down to 2 domain parts (including).
      // TODO This method fails for IPv4 addresses, we should probably detect
      // them and return 'web'.
      urlHostname = urlHostname.split('.');
      for (var i = 0; i <= urlHostname.length - 2; i++) {
        var urlSegment = urlHostname
          .slice(i)
          .join('.');
        if (this.relatedHosts[urlSegment]) {
          // Matching tab was found.
          return 'previous';
        }
      }

      // No match found.
      return 'web';
    }

  });

  /**
   * Base widget editor.
   *
   * @class
   * @extends Contextly.overlayDialog.Base
   */
  Contextly.overlayDialog.BaseWidget = Contextly.createClass(/** @lends Contextly.overlayDialog.BaseWidget.prototype */ {

    extend: Contextly.overlayDialog.Base,

    initState: function() {
      Contextly.overlayDialog.Base.prototype.initState.call(this);

      this.state.removeLinks = {};
      this.state.previewWidgetLink = null;
      this.state.actionsLocked = 0;
      this.state.pendingWidgetLinks = {};
    },

    findElements: function() {
      Contextly.overlayDialog.Base.prototype.findElements.call(this);

      // Sidebar and dialog actions wrapper.
      this.e.sidebar = this.content.find('.sidebar-right');
      this.e.dialogActions = this.e.sidebar.find('.dialog-action');

      // All widgets have the save dialog actions.
      this.e.dialogRemove = this.e.dialogActions.filter('.action-remove');
      this.e.dialogCancel = this.e.dialogActions.filter('.action-cancel');
      this.e.dialogSave = this.e.dialogActions.filter('.action-save');
    },

    attachEventHandlers: function () {
      // TODO Better way to call parent's method?
      Contextly.overlayDialog.Base.prototype.attachEventHandlers.call(this);

      // Link in the widget.
      var sectionLink = this.templates.sectionLink.element;
      var linkTitleEditor = sectionLink.find('.link-title-editor');
      this.bindEnter(linkTitleEditor, this.onTextareaEnter, true);
      var removeLink = sectionLink.find('.remove-link');
      this.bindClick(removeLink, this.onLinkRemove, true);
      var testLink = sectionLink.find('.test-link');
      this.bindClick(testLink, this.onWidgetLinkPreview, true);

      // Dialog actions.
      this.bindDialogAction(this.e.dialogSave, this.onDialogSave);
      this.bindDialogAction(this.e.dialogCancel, this.onDialogCancel);
      this.bindDialogAction(this.e.dialogRemove, this.onDialogRemove);
    },

    /**
     * Binds click on dialog action to the specified callback.
     *
     * One more proxy layer to lock all dialog actions without taking care about
     * it on each callback.
     */
    bindDialogAction: function(element, callback, passArguments) {
      this.bindClick(element, function() {
        if (!this.state.actionsLocked) {
          return callback.apply(this, arguments);
        }
        else {
          return false;
        }
      }, passArguments);
    },

    lockDialogActions: function() {
      this.state.actionsLocked++;
      this.lockControls(this.e.dialogActions, 'dialog-actions');
    },

    unlockDialogActions: function() {
      this.state.actionsLocked--;
      if (this.state.actionsLocked < 0) {
        this.state.actionsLocked = 0;
      }

      if (this.state.actionsLocked == 0) {
        this.unlockControls(this.e.dialogActions, 'dialog-actions');
      }
    },

    performUrlInfoRequest: function(url) {
      // Make sure that the same URL can't be added once again, while previous
      // instance is still pending its title.
      if (this.state.pendingWidgetLinks[url]) {
        return false;
      }

      // Perform the request.
      var started = Contextly.overlayDialog.Base.prototype.performUrlInfoRequest.call(this, url);
      if (!started) {
        return false;
      }

      this.state.pendingWidgetLinks[url] = this.createPendingWidgetLink(url);
      this.refreshDialogActions();
      this.lockDialogActions();

      return true;
    },

    onUrlInfoRequestSuccess: function (urlInfo) {
      if (!this.state.pendingWidgetLinks[urlInfo.url]) {
        // Maybe the link has been removed while we were trying to get its info.
        return;
      }

      var sectionLink = this.state.pendingWidgetLinks[urlInfo.url];
      this.updateSectionLink(sectionLink, {
        native_url: urlInfo.url,
        title: urlInfo.title
      });

      this.refreshDialogActions();
    },

    onUrlInfoRequestError: function (jqXHR) {
      this.displayAlert('Unable to get information about the URL. Something went wrong.');

      // Remove the widget link with failed request.
      var url = jqXHR.contextlyData.url;
      if (this.state.pendingWidgetLinks[url]) {
        var sectionLink = this.state.pendingWidgetLinks[url];
        this.removeWidgetLink(sectionLink);
      }
    },

    onUrlInfoRequestComplete: function(jqXHR) {
      // Remove URL from the list of pending.
      var url = jqXHR.contextlyData.url;
      if (this.state.pendingWidgetLinks[url]) {
        delete this.state.pendingWidgetLinks[url];
      }

      this.unlockDialogActions();
    },

    createPendingWidgetLink: Contextly.abstractMethod(),

    refreshDialogActions: Contextly.abstractMethod(),

    onDialogCancel: function () {
      api.closeOverlay();
    },

    onDialogSave: Contextly.abstractMethod(),

    onDialogRemove: Contextly.abstractMethod(),

    getSortableOptions: function () {
      return {
        handle: '.handle',
        placeholder: '<li class="sortable-placeholder" />'
      };
    },

    updateSectionLink: function(sectionLink, linkData, pending) {
      var titleEditor = sectionLink.find('.link-title-editor');
      var progressIndicator = sectionLink.find('.link-progress-indicator');
      if (pending) {
        titleEditor.hide();
        progressIndicator.show();
      }
      else {
        progressIndicator.hide();
        titleEditor
          .show()
          .text(linkData.title);
        if (linkData.id) {
          sectionLink.attr('data-id', linkData.id);
        }
      }

      return sectionLink;
    },

    createSectionLink: function (linkData, pending) {
      // Create link from the template and fill in permanent data.
      var sectionLink = this.getTemplate('sectionLink')
        .attr('data-url', linkData.native_url);
      sectionLink
        .find('.test-link')
        .attr('href', linkData.native_url);
      return this.updateSectionLink(sectionLink, linkData, pending);
    },

    extractSectionLinkData: function(linkData, sectionLink) {
      var id = sectionLink.attr('data-id');
      if (id) {
        linkData.id = id;
      }

      linkData.url = sectionLink.attr('data-url');
      linkData.title = this.normalizeSpace(sectionLink.find('.link-title-editor').val());

      return linkData;
    },

    onLinkRemove: function(target) {
      var widgetLink = $(target).closest('.section-link');
      this.removeWidgetLink(widgetLink);
    },

    removeWidgetLink: function(sectionLink) {
      // If the link has ID it needs to be removed on the server side too.
      var id = sectionLink.attr('data-id');
      if (id) {
        this.state.removeLinks[id] = true;
      }

      // TODO Check if the section link is pending before removing it from there.
      var url = sectionLink.attr('data-url');
      if (this.state.pendingWidgetLinks[url]) {
        delete this.state.pendingWidgetLinks[url];
      }

      // Remove it from the DOM.
      sectionLink.remove();
    },

    onWidgetLinkPreview: function (target, e) {
      e.preventDefault();

      // Save reference to the widget link for future use.
      this.state.previewWidgetLink = $(target).closest('.section-link')

      // Set up buttons state and attach event handlers.
      this.e.urlPreviewConfirm.hide();
      this.e.urlPreviewRemove
        .unbind('click')
        .show();
      this.bindClick(this.e.urlPreviewRemove, this.onWidgetLinkPreviewRemove);

      // Show the preview.
      this.previewUrl($(target).attr('href'));
    },

    onWidgetLinkPreviewRemove: function () {
      // Get widget link now, because it will be removed by closeUrlPreview().
      var widgetLink = this.state.previewWidgetLink;

      this.closeUrlPreview();
      this.removeWidgetLink(widgetLink);
    },

    closeUrlPreview: function() {
      Contextly.overlayDialog.Base.prototype.closeUrlPreview.call(this);

      // Additionally cleanup reference to the widget link.
      this.state.previewWidgetLink = null;
    }

  });

  /**
   * Snippet editor.
   *
   * @class
   * @extends Contextly.overlayDialog.BaseWidget
   */
  Contextly.overlayDialog.Snippet = Contextly.createClass(/** @lends Contextly.overlayDialog.Snippet.prototype */ {

    extend: Contextly.overlayDialog.BaseWidget,

    findElements: function() {
      // TODO Better way to call parent's method?
      Contextly.overlayDialog.BaseWidget.prototype.findElements.call(this);

      // Snippet result wrapper and sections.
      this.e.sections = this.e.sidebar.find('.sections');
      this.e.snippetResult = this.e.sidebar.find('.snippet-result');
    },

    initState: function() {
      // Call parent method.
      Contextly.overlayDialog.BaseWidget.prototype.initState.call(this);

      // Additionally parse hostname on each tab, for easier search.
      this.indexAnnotations();
    },

    createPendingWidgetLink: function(url) {
      this.e.snippetResult.show();

      var sectionLink = this.createSectionLink({native_url: url}, true);
      var sectionName = this.chooseUrlSection(url);
      this.getSnippetSection(sectionName)
        .find('.section-links')
        .append(sectionLink)
        .sortable('destroy')
        .sortable(this.getSortableOptions());

      return sectionLink;
    },

    renderWidgetLinks: function(links) {
      var result = [];
      for (var i = 0; i < links.length; i++) {
        var sectionLink = this.createSectionLink(links[i]);
        result.push(sectionLink);
      }
      return result;
    },

    getSnippetSection: function(sectionName) {
      var selector = '.section[data-section-name="' + this.escapeSizzleAttrValue(sectionName) + '"]';
      var section = this.e.sections.find(selector);

      if (!section.size()) {
        // No such section yet, build a new one.
        section = this.getTemplate('section')
          .attr('data-section-name', sectionName)
          .find('.section-title')
          .text(this.settings.snippet.settings[sectionName + '_subhead'])
          .end()
          .appendTo(this.e.sections);
      }

      return section;
    },

    initSnippetSections: function() {
      if (!this.settings.snippet || !this.settings.snippet.id) {
        return;
      }
      var snippet = this.settings.snippet;

      for (var sectionName in snippet.links) {
        // Skip "interesting" section, it shouldn't be editable.
        if (sectionName === 'interesting') {
          continue;
        }

        var section = this.getSnippetSection(sectionName);
        var sectionLinksList = section.find('.section-links');
        var sectionLinks = this.renderWidgetLinks(snippet.links[sectionName]);
        sectionLinksList
          .append(sectionLinks)
          .sortable(this.getSortableOptions());
      }

      this.e.snippetResult.show();
    },

    initUI: function() {
      // TODO Better way to call parent's method?
      Contextly.overlayDialog.BaseWidget.prototype.initUI.call(this);

      this.initSnippetSections();
      this.refreshDialogActions();
    },

    removeWidgetLink: function(sectionLink) {
      var sectionLinksList = sectionLink.closest('.section-links');

      Contextly.overlayDialog.BaseWidget.prototype.removeWidgetLink.call(this, sectionLink);

      // Cleanup section or re-init the drag & drop component depending on the
      // number of the links left in section.
      sectionLinksList.sortable('destroy');
      if (sectionLinksList.find('.section-link').size()) {
        sectionLinksList.sortable(this.getSortableOptions());
      }
      else {
        // No links left, remove the section.
        sectionLinksList
          .closest('.section')
          .remove();

        if (!this.e.sections.find('.section').size()) {
          // No more sections left, hide snippet result and refresh buttons.
          this.e.snippetResult.hide();
          this.refreshDialogActions();
        }
      }
    },

    onDialogSave: function() {
      var data = {
        remove_links: [],
        links: []
      };

      // Set widget ID if any.
      if (this.settings.snippet && this.settings.snippet.id) {
        data.snippet_id = this.settings.snippet.id;
      }

      // Build the list of links to remove.
      for (var id in this.state.removeLinks) {
        data.remove_links.push({
          id: id
        });
      }

      // Build the list of links to insert/update.
      var self = this;
      this.e.sections
        .find('.section')
        .each(function() {
          var section = $(this);
          var sectionName = section.attr('data-section-name');
          var pos = 1;
          section
            .find('.section-link')
            .each(function() {
              var linkData = self.extractSectionLinkData({
                type: sectionName,
                pos: pos++
              }, $(this));
              data.links.push(linkData);
            });
        });

      // Perform the request.
      this.showProgressIndicator();
      this.performAjaxRequest('save-snippet', {
        data: data,
        success: this.onDialogSaveSuccess,
        error: this.onDialogSaveError
      });

      return false;
    },

    onDialogSaveSuccess: function(data) {
      api.setSnippet(data);
      api.closeOverlay();
    },

    onDialogSaveError: function () {
      this.displayAlert('Unable to save the widget. Something went wrong.');
      this.hideProgressIndicator();
    },

    onDialogRemove: function() {
      if (window.confirm('The widget will be completely removed. Are you sure?')) {
        if (this.settings.snippet && this.settings.snippet.id) {
          this.showProgressIndicator();
          this.performAjaxRequest('remove-snippet', {
            data: {
              snippet_id: this.settings.snippet.id
            },
            success: this.onDialogRemoveSuccess,
            error: this.onDialogRemoveError
          });
        }
        else {
          // The widget is not yet created. Just call success function directly.
          this.onDialogRemoveSuccess();
        }
      }

      return false;
    },

    onDialogRemoveSuccess: function() {
      if (this.settings.snippet && this.settings.snippet.id) {
        api.removeSnippet(this.settings.snippet.id);
      }

      api.closeOverlay();
    },

    onDialogRemoveError: function() {
      this.displayAlert('Unable to remove the widget. Something went wrong.');
      this.hideProgressIndicator();
    },

    refreshDialogActions: function () {
      // Remove action is available only if the snippet actually available.
      if (this.settings.snippet && typeof this.settings.snippet.id !== 'undefined') {
        this.e.dialogRemove.show();
      }
      else {
        this.e.dialogRemove.hide();
      }

      // Save action is available when there is at least 1 link.
      if (this.e.sections.filter(':has(.section-link)').size()) {
        this.e.dialogSave.show();
      }
      else {
        this.e.dialogSave.hide();
      }
    }

  });

  /**
   * Single link editor.
   *
   * @class
   * @extends Contextly.overlayDialog.Base
   */
  Contextly.overlayDialog.Link = Contextly.createClass(/** @lends Contextly.overlayDialog.Link.prototype */ {

    extend: Contextly.overlayDialog.Base,

    initUI: function() {
      Contextly.overlayDialog.Base.prototype.initUI.call(this);

      // Start searching immediately using selected text passed to the dialog.
      var text = api.getText();
      if (text) {
        this.e.searchInput.val(text);
        this.onSearchSubmit();
      }
    },

    initState: function() {
      // Call parent method.
      Contextly.overlayDialog.Base.prototype.initState.call(this);

      // Additionally parse hostname on each tab, for easier search when we need
      // to add link to the snippet.
      this.indexAnnotations();
    },

    onUrlSubmit: function() {
      var url = this.e.urlInput
        .val()
        .replace(/^\s+|\s+$/, '');
      this.applyUrl(url);
    },

    addSearchResultUrl: function(url) {
      this.applyUrl(url);
    },

    onSearchResultPreviewConfirm: function () {
      // Get URL now, because it will be reset by closeUrlPreview().
      var url = this.state.previewUrl;

      this.closeUrlPreview();
      this.applyUrl(url);
    },

    snippetLinkExists: function(url) {
      var exists = false;

      // Search for the URL in snippet.
      if (this.settings.snippet && this.settings.snippet.links) {
        // Iterate over sections.
        $.each(this.settings.snippet.links, function() {
          if (!this) {
            return;
          }

          // Iterate over section links.
          $.each(this, function() {
            // "this" is a link.
            if (this.native_url === url) {
              exists = true;
              return false;
            }
          });

          if (exists) {
            return false;
          }
        });
      }

      return exists;
    },

    applyUrl: function(url) {
      if (!url) {
        return;
      }

      if (this.snippetLinkExists(url)) {
        // URL is already in snippet. Add immediately.
        this.returnUrl(url);
      }
      else {
        // Add URL to the snippet first.
        this.addSnippetLink({
          url: url
        });
      }
    },

    addSnippetLink: function(data) {
      data.type = this.chooseUrlSection(data.url);

      if (this.settings.snippet) {
        if (typeof this.settings.snippet.id !== 'undefined') {
          data.snippet_id = this.settings.snippet.id;
        }

        // We add link to the end of its section.
        if (this.settings.snippet.links) {
          if (!this.settings.snippet.links) {
            // No links at all on the snippet, just add this one to the first
            // position.
            data.pos = 1;
          }
          else {
            // Find maximum position and use the next one.
            var maxPos = 1;
            $.each(this.settings.snippet.links[data.type], function() {
              if (this.pos > maxPos) {
                maxPos = this.pos;
              }
            });
            data.pos = maxPos + 1;
          }
        }
      }

      this.showProgressIndicator();
      this.performAjaxRequest('add-snippet-link', {
        data: data,
        success: this.onAddSnippetLinkSuccess,
        error: this.onAddSnippetLinkError
      });
    },

    onAddSnippetLinkSuccess: function(response) {
      // Update snippet with new added link.
      api.setSnippet(response.snippet);

      // Pass URL back to the editor.
      this.returnUrl(response.url);
    },

    onAddSnippetLinkError: function() {
      this.displayAlert('Unable to add link to the widget. Something went wrong.');
      this.hideProgressIndicator();
    },

    returnUrl: function(url) {
      if (!url) {
        return;
      }

      api.callback({
        link_url: url,
        link_title: ''
      });
      api.closeOverlay();
    }

  });

  /**
   * Sidebar editor.
   *
   * @class
   * @extends Contextly.overlayDialog.BaseWidget
   */
  Contextly.overlayDialog.Sidebar = Contextly.createClass(/** @lends Contextly.overlayDialog.Sidebar.prototype */ {

    extend: Contextly.overlayDialog.BaseWidget,

    initState: function() {
      Contextly.overlayDialog.BaseWidget.prototype.initState.call(this);

      this.state.id = api.getSidebarId();
      this.state.previewTitle = null;

      if (this.state.id && this.settings.sidebars[this.state.id]) {
        this.state.sidebar = this.settings.sidebars[this.state.id];
        this.state.layout = this.settings.sidebars[this.state.id].layout
      }
      else {
        this.state.sidebar = {};
        this.state.layout = 'left';
      }

      // Register sidebars search type.
      this.searchTypes['sidebars'] = {
        search: this.proxy(this.performSidebarsSearchQuery),
        render: this.proxy(this.renderSidebarsSearchResults)
      };
    },

    findElements: function() {
      // TODO Better way to call parent's method?
      Contextly.overlayDialog.BaseWidget.prototype.findElements.call(this);

      // Sidebar settings.
      var sidebarSettings = this.e.sidebar.find('.sidebar-settings');
      this.e.sidebarTitle = sidebarSettings.find('.sidebar-title');
      this.e.sidebarDescription = sidebarSettings.find('.sidebar-description');
      this.e.sidebarLayoutSwitches = sidebarSettings.find('.sidebar-layout-switch');

      // Modal dialog and its confirmation button.
      this.e.sidebarModal = sidebarSettings.find('.sidebar-modal');
      this.e.sidebarModalConfirm = this.e.sidebarModal.find('.sidebar-modal-confirm');

      // Results wrapper.
      this.e.sidebarResult = this.e.sidebar.find('.sidebar-result');
      this.e.sectionLinks = this.e.sidebarResult.find('.section-links');
    },

    attachEventHandlers: function() {
      Contextly.overlayDialog.BaseWidget.prototype.attachEventHandlers.call(this);

      // Sidebars search tab is not a template and handlers will not be attached
      // on the base class.
      var searchSidebarsTabLink = this.e.searchTabs
        .find('.search-tab[data-search-type="sidebars"] .search-tab-link');
      this.bindClick(searchSidebarsTabLink, this.onSearchTabSwitch, true);

      // Search sidebars result buttons, dropdown menu & links toggles.
      var searchSidebarsResult = this.templates.searchSidebarsResult.element;
      var searchSidebarAdd = searchSidebarsResult
        .find('.search-sidebar-add-all');
      var searchSidebarOverwrite = searchSidebarsResult
        .find('.search-sidebar-overwrite');
      var searchSidebarLinksCollapse = searchSidebarsResult
        .find('.search-sidebar-links-collapse');
      var searchSidebarLinksExpand = searchSidebarsResult
        .find('.search-sidebar-links-expand');
      this.bindClick(searchSidebarAdd, this.onSearchSidebarAddAll, true);
      this.bindClick(searchSidebarOverwrite, this.onSearchSidebarOverwrite, true);
      this.bindClick(searchSidebarLinksCollapse, this.onSearchSidebarLinksCollapse, true);
      this.bindClick(searchSidebarLinksExpand, this.onSearchSidebarLinksExpand, true);

      // Links inside found sidebars.
      var searchSidebarsContentItem = this.templates.searchSidebarsContentItem.element;
      var searchSidebarLinkPreview = searchSidebarsContentItem
        .find('.search-sidebar-link');
      this.bindClick(searchSidebarLinkPreview, this.onSearchSidebarLinkPreview, true);
      var searchSidebarLinkAdd = searchSidebarsContentItem
        .find('.search-sidebar-add-single');
      this.bindClick(searchSidebarLinkAdd, this.onSearchSidebarLinkAdd, true);

      // Layout switch buttons.
      this.bindClick(this.e.sidebarLayoutSwitches, this.onLayoutChange, true);

      // Prevent enter on sidebar description.
      this.bindEnter(this.e.sidebarDescription, this.onTextareaEnter, true);

      // Confirmation button on the modal.
      this.bindClick(this.e.sidebarModalConfirm, this.onSidebarModalConfirm);
    },

    onLayoutChange: function(target) {
      this.state.layout = $(target).attr('data-layout');
      this.refreshLayoutSwitches();
    },

    refreshLayoutSwitches: function() {
      this.e.sidebarLayoutSwitches
        .filter('.active')
        .removeClass('active')
        .end()
        .filter('[data-layout="' + this.escapeSizzleAttrValue(this.state.layout) + '"]')
        .addClass('active');
    },

    initUI: function() {
      Contextly.overlayDialog.BaseWidget.prototype.initUI.call(this);

      this.initSidebarLabels();
      this.refreshLayoutSwitches();
      this.initSidebarLinks();
      this.refreshDialogActions();
    },

    initSidebarLabels: function() {
      if (this.state.id) {
        this.e.sidebarTitle.val(this.state.sidebar.name);
        this.e.sidebarDescription.val(this.state.sidebar.description);
      }
    },

    initSidebarLinks: function() {
      // Sidebar always has single "previous" section.
      if (!this.state.sidebar.links || !this.state.sidebar.links.previous) {
        return;
      }
      var links = this.state.sidebar.links.previous;

      // Put links to the list and init sortable component.
      for (var i = 0; i < links.length; i++) {
        this.e.sectionLinks.append(this.createSectionLink(links[i]));
      }
      this.e.sectionLinks.sortable(this.getSortableOptions());

      this.e.sidebarResult.show();
    },

    createPendingWidgetLink: function(url) {
      this.e.sidebarResult.show();

      var sectionLink = this.createSectionLink({native_url: url}, true);
      this.e.sectionLinks
        .append(sectionLink)
        .sortable('destroy')
        .sortable(this.getSortableOptions());

      return sectionLink;
    },

    refreshDialogActions: function() {
      // Remove action is available only if the sidebar has been saved.
      if (this.state.id) {
        this.e.dialogRemove.show();
      }
      else {
        this.e.dialogRemove.hide();
      }

      // Save action is available when there is at least 1 link.
      if (this.e.sectionLinks.filter(':has(.section-link)').size()) {
        this.e.dialogSave.show();
      }
      else {
        this.e.dialogSave.hide();
      }
    },

    removeWidgetLink: function(sectionLink) {
      Contextly.overlayDialog.BaseWidget.prototype.removeWidgetLink.call(this, sectionLink);

      // Cleanup results or re-init the drag & drop component depending on the
      // number of the links left.
      var sectionLinks = this.e.sectionLinks;
      sectionLinks.sortable('destroy');
      if (sectionLinks.find('.section-link').size()) {
        sectionLinks.sortable(this.getSortableOptions());
      }
      else {
        // No links left, hide the results.
        this.e.sidebarResult.hide();
        this.refreshDialogActions();
      }
    },

    removeAllWidgetLinks: function() {
      this.e.sectionLinks
        .sortable('destroy');

      var sectionLinks = this.e.sectionLinks
        .find('.section-link');
      for (var i = 0; i < sectionLinks.length; i++) {
        var sectionLink = sectionLinks.eq(i);
        Contextly.overlayDialog.BaseWidget.prototype.removeWidgetLink.call(this, sectionLink);
      }
    },

    buildSidebarSettings: function() {
      return {
        name: this.normalizeSpace(this.e.sidebarTitle.val()),
        description: this.normalizeSpace(this.e.sidebarDescription.val()),
        layout: this.state.layout
      };
    },

    onSearchSidebarLinkPreview: function(target, e) {
      e.preventDefault();
      var $target = $(target);

      // Save link title.
      this.state.previewTitle = $target
        .find('.search-sidebar-link-title')
        .text();

      // Set up buttons state and attach event handlers.
      this.e.urlPreviewRemove.hide();
      this.e.urlPreviewConfirm
        .unbind('click')
        .show();
      this.bindClick(this.e.urlPreviewConfirm, this.onSearchSidebarLinkPreviewConfirm);

      // Show the preview.
      this.previewUrl($(target).attr('href'));
    },

    onSearchSidebarLinkPreviewConfirm: function() {
      // Get URL and title now, because it will be reset by closeUrlPreview().
      var url = this.state.previewUrl;
      var title = this.state.previewTitle;

      this.closeUrlPreview();
      this.addSearchSidebarLink(title, url);
    },

    closeUrlPreview: function () {
      Contextly.overlayDialog.BaseWidget.prototype.closeUrlPreview.call(this);

      // Additionally cleanup search sidebar link title.
      this.state.previewTitle = null;
    },

    onSearchSidebarLinkAdd: function(target) {
      var link = $(target)
        .closest('.search-sidebar-content-item')
        .data('contextlyLink');
      this.addSearchSidebarLink(link.title, link.native_url);

      return false;
    },

    addSearchSidebarLink: function(title, url) {
      this.e.sidebarResult.show();

      var sectionLink = this.createSectionLink({
        title: title,
        native_url: url
      });
      this.e.sectionLinks
        .append(sectionLink)
        .sortable('destroy')
        .sortable(this.getSortableOptions());

      this.refreshDialogActions();
    },

    addSearchSidebarLinks: function(sidebar) {
      this.e.sidebarResult.show();

      for (var i = 0; i < sidebar.links.previous.length; i++) {
        var link = sidebar.links.previous[i];
        var sectionLink = this.createSectionLink({
          title: link.title,
          native_url: link.native_url
        });
        this.e.sectionLinks.append(sectionLink);
      }

      this.e.sectionLinks
        .sortable('destroy')
        .sortable(this.getSortableOptions());
    },

    onSearchSidebarAddAll: function(target) {
      var $target = $(target);

      var sidebar = $target
        .closest('.search-sidebars-result')
        .data('contextlySidebar');

      this.addSearchSidebarLinks(sidebar);
      this.refreshDialogActions();

      this.closeSearchSidebarDropdown($target);

      return false;
    },

    onSearchSidebarOverwrite: function(target) {
      var $target = $(target);

      // Fix links list height before making any changes to avoid scroll
      // position changes.
      this.e.sectionLinks.css('height', this.e.sectionLinks.height());

      var sidebar = $target
        .closest('.search-sidebars-result')
        .data('contextlySidebar');

      // Cleanup all links and add all links from the search result.
      this.removeAllWidgetLinks();
      this.addSearchSidebarLinks(sidebar);

      // Copy name, description and alignment.
      this.e.sidebarTitle.val(sidebar.name);
      this.e.sidebarDescription.val(sidebar.description);
      this.state.layout = sidebar.layout;

      this.refreshLayoutSwitches();
      this.refreshDialogActions();

      // Cleanup the height fix.
      this.e.sectionLinks.css('height', '');

      this.closeSearchSidebarDropdown($target);

      return false;
    },

    closeSearchSidebarDropdown: function($target) {
      var dropdown = $target.closest('.dropdown-menu');
      if (dropdown.size()) {
        dropdown
          .parent()
          .removeClass('open');
      }
    },

    onDialogSave: function () {
      var sidebar = this.buildSidebarSettings();

      if (!sidebar.name && !sidebar.description) {
        this.e.sidebarModal.modal();
      }
      else {
        this.saveSidebar(sidebar);
      }

      return false;
    },

    onSidebarModalConfirm: function() {
      this.e.sidebarModal.modal('hide');
      var sidebar = this.buildSidebarSettings();
      this.saveSidebar(sidebar);
    },

    saveSidebar: function(sidebar) {
      var data = {
        sidebar: sidebar,
        remove_links: [],
        links: []
      };

      // Set sidebar ID if any.
      if (this.state.id) {
        data.sidebar.id = this.state.id;
      }

      // Build the list of links to remove.
      for (var id in this.state.removeLinks) {
        data.remove_links.push({
          id: id
        });
      }

      // Build the list of links to insert/update.
      var self = this;
      var pos = 1;
      this.e.sectionLinks
        .find('.section-link')
        .each(function () {
          var linkData = self.extractSectionLinkData({
            type: 'previous',
            pos: pos++
          }, $(this));

          data.links.push(linkData);
        });

      // Perform the request.
      this.showProgressIndicator();
      this.performAjaxRequest('save-sidebar', {
        data: data,
        success: this.onDialogSaveSuccess,
        error: this.onDialogSaveError
      });
    },

    onDialogSaveSuccess: function (sidebar) {
      // Call the callback. It can prevent the sidebar update by returning false.
      var callbackResult = api.callback(sidebar);
      if (callbackResult !== false) {
        api.setSidebar(sidebar);
      }

      api.closeOverlay();
    },

    onDialogSaveError: function () {
      this.displayAlert('Unable to save the sidebar. Something went wrong.');
      this.hideProgressIndicator();
    },

    onDialogRemove: function () {
      if (window.confirm('The sidebar will be completely removed. Are you sure?')) {
        if (this.state.id) {
          this.showProgressIndicator();
          this.performAjaxRequest('remove-sidebar', {
            data: {
              sidebar_id: this.state.id
            },
            success: this.onDialogRemoveSuccess,
            error: this.onDialogRemoveError
          });
        }
        else {
          // The sidebar is not yet created. Just call success function directly.
          this.onDialogRemoveSuccess();
        }
      }

      return false;
    },

    onDialogRemoveSuccess: function () {
      if (this.state.id) {
        api.removeSidebar(this.state.id);
      }

      api.closeOverlay();
    },

    onDialogRemoveError: function () {
      this.displayAlert('Unable to remove the sidebar. Something went wrong.');
      this.hideProgressIndicator();
    },

    performSidebarsSearchQuery: function (searchInfo, query, page) {
      var data = {
        type: searchInfo.type,
        query: query,
        page: page
      };

      if (this.state.id) {
        data.sidebar_id = this.state.id;
      }

      this.performAjaxRequest('search', {
        data: data,
        success: this.onSearchRequestSuccess,
        error: this.onSearchRequestError,
        complete: this.hideProgressIndicator
      });
    },

    onSearchSidebarLinksCollapse: function(target) {
      var collapse = $(target);
      var toggles = collapse.closest('.search-sidebar-links-toggles');
      var expand = toggles.find('.search-sidebar-links-expand');

      collapse.hide();
      expand.show();

      var links = toggles
        .closest('.search-result-info')
        .find('.search-sidebar-content-item');

      links
        .slice(this.settings.sidebarsSearchLinksLimit)
        .hide();

      return false;
    },

    onSearchSidebarLinksExpand: function(target) {
      var expand = $(target);
      var toggles = expand.closest('.search-sidebar-links-toggles');
      var collapse = toggles.find('.search-sidebar-links-collapse');

      expand.hide();
      collapse.show();

      var links = toggles
        .closest('.search-result-info')
        .find('.search-sidebar-content-item');

      links
        .slice(this.settings.sidebarsSearchLinksLimit)
        .show();

      return false;
    },

    renderSidebarsSearchResults: function (sidebars) {
      var results = [];
      if (sidebars && sidebars.length) {
        for (var i = 0; i < sidebars.length; i++) {
          var sidebar = sidebars[i];

          var searchResult = this.getTemplate('searchSidebarsResult')
            .attr('data-sidebar-id', sidebar.id)
            .data('contextlySidebar', sidebar);

          // Fill title and description.
          var titleElement = searchResult.find('.search-result-title');
          if (sidebar.name) {
            titleElement.text(sidebar.name);
          }
          else {
            titleElement.hide();
          }
          var descriptionElement = searchResult.find('.search-result-description');
          if (sidebar.description) {
            descriptionElement.text(sidebar.description);
          }
          else {
            descriptionElement.hide();
          }

          // Fill the list of links. Server side should filter-out sidebars
          // without links.
          var linksList = searchResult.find('.search-sidebar-content');
          for (var j = 0; j < sidebar.links.previous.length; j++) {
            var link = sidebar.links.previous[j];
            var item = this.getTemplate('searchSidebarsContentItem')
              .data('contextlyLink', link);
            item
              .find('.search-sidebar-link')
              .attr('href', link.native_url)
              .find('.search-sidebar-link-title')
              .text(link.title);
            linksList.append(item);
          }

          // Collapse links by default if there are more than limit + 1 links.
          if (sidebar.links.previous.length > this.settings.sidebarsSearchLinksLimit + 1) {
            searchResult
              .find('.search-sidebar-links-toggles')
              .show();

            // We can't just trigger the click handler, because the dialog is
            // still in progress. Just call it directly on the collapse link.
            var collapse = searchResult.find('.search-sidebar-links-collapse');
            this.onSearchSidebarLinksCollapse(collapse[0]);
          }

          // Dropdown has to be initialised on the clone, because Bootstrap
          // plugin requires the menu to be in the DOM tree and doesn't support
          // cloning instances.
          searchResult
            .find('.search-sidebar-actions-toggle')
            .dropdown();

          results.push(searchResult);
        }
      }
      return results;
    }

  });

  /**
   * Search info storage.
   *
   * @class
   */
  Contextly.overlayDialog.SearchInfo = Contextly.createClass( /** @lends Contextly.overlayDialog.SearchInfo.prototype */ {
    construct: function(type, siteUrl) {
      this.type = type;
      this.siteUrl = siteUrl;
    },

    /**
     * Returns string represetnation of the search info.
     *
     * @todo Rename to toString() and use JS magic instead of direct call when
     *   we drop IE 8 support.
     *
     * @returns {string}
     */
    toStr: function() {
      return this.type + ':' + this.siteUrl;
    }
  });

  /**
   * Shortcut to access jQuery of the parent window.
   *
   * It is set by the DOM ready callback.
   */
  var p$ = null;

  /**
   * API to communicate with the parent window callbacks.
   *
   * Filled by the parent window event handler.
   */
  var api = {};

  /**
   * Main entry point, DOM ready callback.
   */
  $(function() {
    if (!window.parent || !window.parent.jQuery) {
      $.error('Unable to connect to the parent window.');
    }

    // Set shortcut for jQuery of the parent window.
    p$ = window.parent.jQuery;

    // Let the parent window know that iframe is ready to work and to fill the
    // API object.
    api = p$(window.parent).triggerHandler('contextlyOverlayReady');
    if (typeof api === 'undefined') {
      $.error('Unable to communicate with the parent window.');
    }

    var content = $('.contextly-editor');
    var type = content.attr('data-editor-type');
    var editor = null;
    switch (type) {
      case 'snippet':
        editor = new Contextly.overlayDialog.Snippet(content);
        break;

      case 'link':
        editor = new Contextly.overlayDialog.Link(content);
        break;

      case 'sidebar':
        editor = new Contextly.overlayDialog.Sidebar(content);
        break;

      default:
        $.error('Unknown type of the overlay editor.');
    }
  });

})(jQuery);
