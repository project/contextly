(function ($) {

  Contextly = window.Contextly || {};

  /**
   * Common features for the links/sidebars editing.
   */
  Contextly.editor = {
    isLoaded: false,
    isLoading: false,

    open: function(type) {
      var func = '';
      switch (type) {
        case 'snippet':
          func = this.openGeneralEditor;
          break;

        case 'link':
          func = this.openLinkEditor;
          break;

        case 'sidebar':
          func = this.openSidebarEditor;
          break;

        default:
          return;
      }

      // Remove the type from the arguments.
      var args = Array.prototype.slice.call(arguments, 1);

      // Avoid editor start when loading is still in progress.
      if (!this.isLoaded) {
        // Load settings if not yet started.
        if (!this.isLoading) {
          this.loadSettings();
        }
      }
      else {
        func.apply(this, args);
      }
    },

    openGeneralEditor: function() {
      var s = this.getSettings();
      var url = this.buildEditorUrl('snippet', s.nid);
      this.openOverlay(url, {
        setSnippet: this.proxy(this.setSnippet),
        removeSnippet: this.proxy(this.removeSnippet)
      });
    },

    openLinkEditor: function(text, callback, context) {
      var s = this.getSettings();
      var url = this.buildEditorUrl('link', s.nid);
      this.openOverlay(url, {
        callback: this.proxy(callback, context),
        setSnippet: this.proxy(this.setSnippet),
        getText: function() {
          return text;
        }
      });
    },

    openSidebarEditor: function(sidebarId, callback, context) {
      var s = this.getSettings();
      var url = this.buildEditorUrl('sidebar', s.nid);
      this.openOverlay(url, {
        getSidebarId: function() {
          return sidebarId;
        },
        callback: this.proxy(callback, context),
        setSidebar: this.proxy(this.setSidebar),
        removeSidebar: this.proxy(this.removeSidebar)
      });
    },

    openOverlay: function(url, api, optionOverrides) {
      var options = $.extend({
        width: {
          max: 1400
        }
      }, optionOverrides);

      // Extend API with default methods (caller is able to overwrite them).
      api = $.extend({
        getSettings: this.proxy(this.getSettings),
        buildAjaxUrl: this.proxy(this.buildAjaxUrl),
        setOverlayCloseButtonHandler: function(callback) {
          Contextly.overlay.setCloseButtonHandler(callback);
        },
        closeOverlay: function() {
          Contextly.overlay.close();
        }
      }, api);

      // Set up event handler to respond on overlay ready events with an API.
      $(window)
        .unbind('contextlyOverlayReady')
        .bind('contextlyOverlayReady', function() {
          return api;
        });

      // Load an iframe inside modal.
      Contextly.overlay.open($('<iframe frameBorder="0"/>').attr({
        src: url,
        width: '100%',
        height: '100%'
      }), options);
    },

    loadSettings: function() {
      var s = this.getSettings();

      this.isLoaded = false;
      this.isLoading = true;

      $.ajax({
        url: this.buildAjaxUrl('node-edit', s.nid),
        success: this.onSettingsLoadingSuccess,
        error: this.onSettingsLoadingFailure,
        cache: false,
        context: this
      });

      this.fireEvent('contextlySettingsLoading');
    },

    getSettings: function() {
      return $.extend({
        nid: 0,
        token: '',
        baseUrl: '',
        sidebars: {},
        snippet: null,
        annotations: [],
        sidebarsSearchLinksLimit: 4
      }, Drupal.settings.contextlyEditor);
    },

    buildAjaxUrl: function(method, nid) {
      var url = Drupal.settings.basePath + 'contextly-ajax/' + method + '/' + nid;
      return this.appendUrlToken(url);
    },

    buildEditorUrl: function(type, nid) {
      var url = Drupal.settings.basePath + 'contextly-editor/' + type + '/' + nid;
      return this.appendUrlToken(url);
    },

    appendUrlToken: function(url) {
      if (url.indexOf('?') === -1) {
        url += '?';
      }
      else {
        url += '&';
      }

      var s = this.getSettings();
      url += 'token=' + encodeURIComponent(s.token);

      return url;
    },

    onSettingsLoadingSuccess: function(data) {
      // Set flags.
      this.isLoaded = true;
      this.isLoading = false;

      // Put new data into the settings.
      $.extend(Drupal.settings.contextlyEditor, data);

      // Let the other scripts to know about loaded settings.
      this.fireEvent('contextlySettingsLoaded');
    },

    onSettingsLoadingFailure: function() {
      this.fireEvent('contextlySettingsFailed');
    },

    fireEvent: function(type) {
      // Remove the type of event.
      var args = Array.prototype.slice.call(arguments, 1);

      $(window).triggerHandler(type, args);
    },

    setSidebar: function(entry) {
      Drupal.settings.contextlyEditor.sidebars[entry.id] = entry;
      this.fireEvent('contextlyWidgetUpdated', 'sidebar', entry.id);
    },

    setSnippet: function(entry) {
      Drupal.settings.contextlyEditor.snippet = entry;
      this.fireEvent('contextlyWidgetUpdated', 'snippet', entry.id);
    },

    removeSnippet: function(id) {
      // On empty snippet we still need settings for editor to work properly.
      var emptySnippet = {
        settings: Drupal.settings.contextlyEditor.snippet.settings
      };
      Drupal.settings.contextlyEditor.snippet = emptySnippet;
      this.fireEvent('contextlyWidgetRemoved', 'snippet', id);
    },

    removeSidebar: function(id) {
      delete Drupal.settings.contextlyEditor.sidebars[id];
      this.fireEvent('contextlyWidgetRemoved', 'sidebar', id);
    },

    proxy: function(func, context) {
      if (typeof context === 'undefined') {
        context = this;
      }

      return function() {
        return func.apply(context, arguments);
      };
    }

  };

  /**
   * Behavior to load the settings on the DOM ready.
   */
  Drupal.behaviors.contextlyEditor = {
    attach: function(context, settings) {
      $('body', context).once('contextly-editor', function() {
        Contextly.editor.loadSettings();
      });
    }
  };

})(jQuery);
