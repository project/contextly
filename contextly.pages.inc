<?php

/**
 * Callback for the settings loading on the node edit page.
 *
 * @param stdClass $node
 */
function contextly_ajax_node_edit_callback($node) {
  try {
    $contextly = Contextly::getInstance();
    $settings = $contextly->loadEditorSettings($node);
  }
  catch (ContextlyException $e) {
    watchdog_exception('contextly', $e);
    throw $e;
  }

  drupal_json_output($settings);
}

function contextly_snippet_editor_page($node) {
  $content = array(
    '#theme' => 'contextly_snippet_editor',
    '#attached' => array(
      'library' => array(
        array('contextly', 'editor-ui'),
      ),
    ),
  );
  return $content;
}

function contextly_link_editor_page($node) {
  $content = array(
    '#theme' => 'contextly_link_editor',
    '#attached' => array(
      'library' => array(
        array('contextly', 'editor-ui'),
      ),
    ),
  );
  return $content;
}

function contextly_sidebar_editor_page($node) {
  $content = array(
    '#theme' => 'contextly_sidebar_editor',
    '#attached' => array(
      'library' => array(
        array('contextly', 'editor-ui'),
      ),
    ),
  );
  return $content;
}

function contextly_ajax_search_callback($node) {
  _contextly_check_required_post_params(array('type', 'query', 'page'), 'search');

  try {
    $contextly = Contextly::getInstance();
    $limit = variable_get('contextly_search_results_limit', 10);
    $type = $_POST['type'];
    switch ($type) {
      case 'links':
        _contextly_check_required_post_params(array('site_url'), 'search');
        $result = $contextly->searchLinks($_POST['site_url'], $_POST['query'], $_POST['page'], $limit);
        break;

      case 'sidebars':
        $sidebar_id = isset($_POST['sidebar_id']) ? $_POST['sidebar_id'] : NULL;
        $result = $contextly->searchSidebars($sidebar_id, $_POST['query'], $_POST['page'], $limit);
        break;

      default:
        watchdog('contextly', 'Unknown search type %type.', array(
          '%type' => $type,
        ), WATCHDOG_ERROR);
        _contextly_return_bad_request();
        break;
    }

    // Normalize URL to be at least empty string.
    if (empty($result['siteUrl'])) {
      $result['siteUrl'] = '';
    }

    // Return type back.
    $result['type'] = $type;
  }
  catch (ContextlyException $e) {
    watchdog_exception('contextly', $e);
    throw $e;
  }

  drupal_json_output($result);
}

function contextly_ajax_url_info_callback($node) {
  // TODO Check required parameters and return appropriate error.

  try {
    $contextly = Contextly::getInstance();
    $info = $contextly->fetchUrlInfo($_POST['url']);
  }
  catch (ContextlyException $e) {
    watchdog_exception('contextly', $e);
    throw $e;
  }

  drupal_json_output($info);
}

function contextly_ajax_save_snippet_callback($node) {
  // TODO Check required parameters.

  try {
    $data = array(
      'custom_id' => $node->nid,
    );

    $optional_params = array('links', 'remove_links', 'snippet_id');
    foreach ($optional_params as $param) {
      if (isset($_POST[$param])) {
        $data[$param] = $_POST[$param];
      }
    }

    $contextly = Contextly::getInstance();
    $snippet = $contextly->saveSnippet($data);
  }
  catch (ContextlyException $e) {
    watchdog_exception('contextly', $e);
    throw $e;
  }

  drupal_json_output($snippet);
}

function contextly_ajax_remove_snippet_callback($node) {
  // TODO Check required parameters.

  try {
    $contextly = Contextly::getInstance();
    $contextly->removeSnippet($_POST['snippet_id']);
  }
  catch (ContextlyException $e) {
    watchdog_exception('contextly', $e);
    throw $e;
  }

  drupal_json_output(array('success' => TRUE));
}

function contextly_ajax_save_sidebar_callback($node) {
  // TODO Check required parameters.

  try {
    $data = array(
      'sidebar' => array(
        'custom_id' => $node->nid,
      ),
    );

    $sidebar_params = array(
      'id' => FALSE,
      'name' => TRUE,
      'description' => TRUE,
      'layout' => TRUE,
    );
    foreach ($sidebar_params as $param => $is_required) {
      if ($is_required || isset($_POST['sidebar'][$param])) {
        $data['sidebar'][$param] = $_POST['sidebar'][$param];
      }
    }

    $optional_params = array('links', 'remove_links');
    foreach ($optional_params as $param) {
      if (isset($_POST[$param])) {
        $data[$param] = $_POST[$param];
      }
    }

    $contextly = Contextly::getInstance();
    $sidebar = $contextly->saveSidebar($data);
  }
  catch (ContextlyException $e) {
    watchdog_exception('contextly', $e);
    throw $e;
  }

  drupal_json_output($sidebar);
}

function contextly_ajax_remove_sidebar_callback($node) {
  // TODO Check required parameters.

  try {
    $contextly = Contextly::getInstance();
    $contextly->removeSidebar($_POST['sidebar_id']);
  }
  catch (ContextlyException $e) {
    watchdog_exception('contextly', $e);
    throw $e;
  }

  drupal_json_output(array('success' => TRUE));
}

function contextly_ajax_add_snippet_link_callback($node) {
  // TODO Check required parameters.

  try {
    $response = array();
    $data = array(
      'custom_id' => $node->nid,
    );

    $params = array(
      'url' => TRUE,
      'title' => FALSE,
      'type' => TRUE,
      'pos' => TRUE,
      'snippet_id' => FALSE,
    );
    foreach ($params as $param => $is_required) {
      if ($is_required || isset($_POST[$param])) {
        $data[$param] = $_POST[$param];
        $response[$param] = $_POST[$param];
      }
    }

    $contextly = Contextly::getInstance();
    $response['snippet'] = $contextly->addSnippetLink($data);
  }
  catch (ContextlyException $e) {
    watchdog_exception('contextly', $e);
    throw $e;
  }

  drupal_json_output($response);
}

function _contextly_check_required_post_params($keys, $call_type) {
  foreach ($keys as $key) {
    if (!isset($_POST[$key])) {
      watchdog('contextly', 'Missing parameter %key on @type call.', array(
        '%key' => $key,
        '@type' => $call_type,
      ), WATCHDOG_ERROR);
      _contextly_return_bad_request();
    }
  }
}

function _contextly_return_bad_request() {
  drupal_add_http_header('Status', '400 Bad Request');
  drupal_exit();
}
