<?php

/**
 * Returns Contextly settings URL of specified type.
 *
 * @param string $type
 * @param null|string $access_token
 *   Is used to build direct login URL when specified.
 *
 * @return string
 */
function _contextly_settings_url($type, $access_token = NULL) {
  $query = array(
    'type' => $type,
  );

  if (isset($access_token)) {
    $query += array(
      'contextly_access_token' => $access_token,
    );
  }
  else {
    $query += array(
      'blog_url' => $GLOBALS['base_url'],
      'blog_title' => variable_get('site_name', 'Drupal'),
    );
  }

  return url(_contextly_server_url('cp') . 'redirect/', array('query' => $query));
}

/**
 * Settings form.
 */
function contextly_settings_form($form, &$state) {
  $key = variable_get('contextly_api_key', array());

  $form['key'] = array(
    '#type' => 'fieldset',
    '#title' => t('API key'),
    '#collapsible' => TRUE,
    '#collapsed' => !empty($key),
  );

  $form['key']['link'] = array(
    '#type' => 'item',
    '#markup' => t('In order to communicate securely, we use a shared secret key. You can find your secret API key on !home_link. Copy and paste it below.', array(
      '!home_link' => l(t("Contextly's settings panel"), _contextly_settings_url('tour'), array(
        'attributes' => array(
          'target' => '_blank',
        )
      )),
    )),
  );

  $form['key']['contextly_api_key'] = array(
    '#required' => TRUE,
    '#type' => 'textfield',
    '#title' => t('Secret API key'),
    '#default_value' => implode('-', $key),
  );

  $form = system_settings_form($form);

  // Move submit button to the API key fieldset and remove empty actions.
  $form['key']['submit'] = $form['actions']['submit'];
  unset($form['actions']);

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Service settings'),
    '#collapsible' => TRUE,
    '#collapsed' => empty($key),
  );
  $form['settings']['notice'] = array(
    '#type' => 'item',
    '#markup' => t('The majority of the settings for Contextly are handled outside of Drupal. Click the settings button to securely login to your settings panel. If that fails, you can still <a href="@url" target="_blank">login via Twitter</a>.', array(
      '@url' => _contextly_settings_url('settings'),
    )),
  );
  $form['settings']['go'] = array(
    '#type' => 'submit',
    '#value' => t('Settings'),
    '#attributes' => array(
      'class' => array('contextly-settings-button'),
    ),
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'contextly') . '/js/contextly-settings-form.js',
      ),
    ),
    '#submit' => array('contextly_settings_form_submit_contextly_settings'),

    // Empty validation to avoid unnecessary default form validation. Required
    // field needs limit_validation_errors anyway.
    '#validate' => array(),
    '#limit_validation_errors' => array(),
  );

  return $form;
}

/**
 * Validation for the settings form.
 */
function contextly_settings_form_validate($form, &$state) {
  $key = trim($state['values']['contextly_api_key']);

  // Parse string key into an array.
  $key = explode('-', $key, 2);
  if (count($key) !== 2) {
    form_set_error('contextly_api_key', t('API key is incorrect.'));
    return;
  }

  // Convert it into associative array for later use.
  $key = array_combine(array('appID', 'appSecret'), $key);

  try {
    // Check API key. Create API client with isolated session.
    $api = new ContextlyApi(new ContextlySessionIsolated(), $key);

    // Failed authorization should throw an exception at this point.
    $api->connect();

    // Save parsed key back to values.
    $state['values']['contextly_api_key'] = $key;
  }
  catch (Exception $e) {
    form_set_error('contextly_api_key', t('Test API request failed.'));
  }
}

/**
 * Submit handler for the "Contextly settings" button of the settings form.
 *
 * Redirects to Contextly service settings page.
 */
function contextly_settings_form_submit_contextly_settings($form, &$state) {
  // Try to get the auth token and use direct login.
  $token = NULL;
  try {
    $api = ContextlyApi::getInstance();
    $api->connect();
    $session = $api->getSession();
    $token = $session->getAccessToken();
  }
  catch (Exception $e) {
    // Just silently fall back to the Twitter login.
  }

  unset($_GET['destination']);
  $state['redirect'] = _contextly_settings_url('settings', $token);
}

/**
 * Contextly-enabled node types settings form.
 */
function contextly_settings_node_types_form($form, &$state) {
  $types = node_type_get_names();
  $types = array_map('check_plain', $types);

  $form['types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enabled node types'),
  );

  $form['types']['info'] = array(
    '#type' => 'item',
    '#markup' => t("Integration with Contextly will be enabled for selected node types only."), // TODO Move to help
  );

  $form['types']['contextly_all_node_types'] = array(
    '#type' => 'checkbox',
    '#title' => t('All node types.'),
    '#default_value' => variable_get('contextly_all_node_types', TRUE),
  );

  $form['types']['contextly_node_types'] = array(
    '#title' => t('Enabled types'),
    '#type' => 'checkboxes',
    '#options' => $types,
    '#default_value' => variable_get('contextly_node_types', array()),
    '#element_validate' => array('_contextly_multi_value_element_cleanup'),
    '#states' => array(
      'visible' => array(
        'input[name="contextly_all_node_types"]' => array(
          'checked' => FALSE,
        ),
      ),
    ),
  );

  $form = system_settings_form($form);

  // Custom handler to clear fields cache on submit.
  $form['#submit'][] = 'contextly_settings_node_types_form_submit_clear_cache';

  return $form;
}

/**
 * Submit handler for the node types settings form to clear field cache.
 *
 * Field cache must be cleared for Contextly widget to appear/disappear on the
 * node display management UI.
 */
function contextly_settings_node_types_form_submit_clear_cache($form, &$state) {
  field_cache_clear();
}

/**
 * Removes empty (not selected) values from multi-value form elements.
 */
function _contextly_multi_value_element_cleanup($element, &$state, $form) {
  $filtered_value = array();
  if (!empty($element['#value'])) {
    $filtered_value = array_filter($element['#value']);
  }
  form_set_value($element, $filtered_value, $state);
}

/**
 * Returns the list of field instances on Contextly-enabled node types.
 *
 * @param array $field_types
 *
 * @return array
 *   List of field names indexed by node type.
 */
function _contextly_settings_get_available_fields(array $field_types) {
  $enabled_types = _contextly_get_enabled_types();
  $available_fields = array();
  foreach ($enabled_types as $node_type) {
    $node_type_fields = _contextly_get_node_type_fields($node_type, $field_types);
    if (!empty($node_type_fields)) {
      $available_fields[$node_type] = $node_type_fields;
    }
  }
  return $available_fields;
}

function contextly_settings_tags_form($form, $state) {
  $form['tags'] = array(
    '#type' => 'fieldset',
    '#title' => t('Taxonomy terms'),
  );

  $form['tags']['info'] = array(
    '#type' => 'item',
    '#markup' => t('Terms from selected fields will be sent to the Contextly.'), // TODO Move to help.
  );

  $form['tags']['contextly_all_tags'] = array(
    '#type' => 'checkbox',
    '#title' => t('All term reference fields'),
    '#default_value' => variable_get('contextly_all_tags', TRUE),
  );

  $available_fields = _contextly_settings_get_available_fields(array('taxonomy_term_reference'));
  $node_types = node_type_get_types();
  foreach ($available_fields as $type => $fields) {
    $options = array();
    foreach ($fields as $field_name) {
      $instance = field_info_instance('node', $field_name, $type);
      $options[$field_name] = check_plain($instance['label']);
    }

    $node_type = $node_types[$type];
    $form['tags']['contextly_tags__' . $type] = array(
      '#type' => 'checkboxes',
      '#title' => check_plain($node_type->name),
      '#options' => $options,
      '#default_value' => variable_get('contextly_tags__' . $type, array()),
      '#element_validate' => array('_contextly_multi_value_element_cleanup'),
      '#states' => array(
        'visible' => array(
          'input[name="contextly_all_tags"]' => array(
            'checked' => FALSE,
          ),
        ),
      ),
    );
  }

  return system_settings_form($form);
}
