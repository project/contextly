<?php

/**
 * Helper for the Contextly service integration.
 */
class Contextly {

  protected function __construct() {}

  static public function getInstance() {
    static $instance;

    if (!isset($instance)) {
      $instance = new self();
    }

    return $instance;
  }

  /**
   * Sends the node to Contextly.
   *
   * @param object $node
   */
  public function putNode($node) {
    $this->putNodeContent($node);
    if (module_exists('taxonomy')) {
      $this->putNodeTags($node);
    }
  }

  /**
   * Sends the node text content and meta-information to Contextly.
   *
   * @param object $node
   */
  protected function putNodeContent($node) {
    // Build absolute node URL.
    $uri = entity_uri('node', $node);
    if (!isset($uri['path'])) {
      throw new ContextlyException(t('Unable to generate URL for the node #@nid.', array(
        '@nid' => $node->nid,
      )));
    }
    $uri['options']['absolute'] = TRUE;
    $node_url = url($uri['path'], $uri['options']);

    $api = ContextlyApi::getInstance();

    // Check if post has been saved to Contextly earlier.
    $contextly_post = $api->api('posts', 'get')
      ->param('page_id', $node->nid)
      ->get();

    $user = user_load($node->uid);

    // TODO Take care about langcode.
    $content = node_view($node, 'contextly');
    $content = render($content);

    $post_data = array(
      'post_id' => $node->nid,
      'post_title' => $node->title,
      'post_date' => format_date($node->created, 'custom', 'Y-m-d H:i:s'),
      'post_modified' => format_date($node->changed, 'custom', 'Y-m-d H:i:s'),
      'post_status' => $node->status ? 'publish' : 'draft',
      'post_type' => $node->type,
      'post_content' => $content,
      'url' => $node_url,
      'author_id' => $node->uid,
      'post_author' => $user->name,
    );
    $api
      ->api('posts', 'put')
      ->extraParams($post_data);
    if (isset($contextly_post->entry)) {
      $api->param('id', $contextly_post->entry->id);
    }

    $response = $api->get();
    if (empty($response->success)) {
      throw new ContextlyApiException(t('Unable to send content of the node #@nid to the Contextly.', array(
        '@nid' => $node->nid,
      )), $response);
    }
  }

  /**
   * Extracts tags list attached to the node that should be sent to Contextly.
   *
   * @param object $node
   *
   * @return array
   */
  protected function extractNodeTags($node) {
    $tags = array();

    // Use either all available fields or selected fields only.
    if (variable_get('contextly_all_tags', TRUE)) {
      $fields = _contextly_get_node_type_fields($node->type, array('taxonomy_term_reference'));
    }
    else {
      $fields = array_values(variable_get('contextly_tags__' . $node->type, array()));
    }

    // Collect all term IDs first.
    $tids = array();
    foreach ($fields as $field_name) {
      if (!empty($node->{$field_name})) {
        // TODO: Handle multi-language fields properly.
        // For now just post all languages.
        foreach ($node->{$field_name} as $lang => $items) {
          foreach ($items as $item) {
            $tid = $item['tid'];
            $tids[$tid] = TRUE;
          }
        }
      }
    }

    if (!empty($tids)) {
      $terms = taxonomy_term_load_multiple(array_keys($tids));
      foreach ($terms as $term) {
        $tags[] = $term->name;
      }
    }

    return $tags;
  }

  /**
   * Sends node tags to the Contextly.
   *
   * @param object $node
   */
  protected function putNodeTags($node) {
    $api = ContextlyApi::getInstance();

    // Remove existing tags first, if any.
    // TODO: Handle pagination of the request.
    $post_tags = $api->api('poststags', 'list')
      ->searchParam('post_id', ContextlyApi::SEARCH_TYPE_EQUAL, $node->nid)
      ->get();
    if (!empty($post_tags->list)) {
      foreach ($post_tags->list as $tag) {
        $api->api('poststags', 'delete')
          ->param('id', $tag->id)
          ->get();
        // TODO Check response.
      }
    }

    // Save new tags.
    $tags = $this->extractNodeTags($node);
    // TODO WP Plugin sends only 3 first tags. Why?
    foreach ($tags as $tag) {
      $tag_data = array(
        'post_id' => $node->nid,
        'name' => $tag,
      );

      $response = $api->api('poststags', 'put')
        ->extraParams($tag_data)
        ->get();
      if (empty($response->success)) {
        throw new ContextlyApiException(t('Unable to save tag @name', array('@name' => $tag)), $response);
      }
    }
  }

  /**
   * Loads page widgets from Contextly.
   *
   * @param int $nid
   * @param bool $admin
   * @param bool $editor
   *
   * @return object
   *
   * @throws ContextlyApiException
   */
  protected function loadPageWidgets($nid, $admin, $editor) {
    $response = ContextlyApi::getInstance()
      ->api('pagewidgets', 'get')
      ->param('page_id', $nid)
      ->param('admin', $admin ? 1 : 0)
      ->param('editor', $editor ? 1 : 0)
      ->get();
    if (!empty($response->success) || !empty($response->entry)) {
      return $response->entry;
    }
    else {
      throw new ContextlyApiException(t('Unable to load page widgets.'), $response);
    }
  }

  /**
   * Returns JS settings for the node edit form.
   *
   * @param $node
   *
   * @return array
   */
  public function buildEditorSettings($node) {
    return array(
      'contextlyEditor' => array(
        'token' => drupal_get_token(_contextly_node_edit_token_value($node)),
        'nid' => $node->nid,
        'baseUrl' => $GLOBALS['base_url'] . '/',
      ),
    );
  }

  /**
   * Loads required settings from Contextly for the node edit form.
   *
   * @param stdClass $node
   *
   * @return array
   */
  public function loadEditorSettings($node) {
    $widgets = $this->loadPageWidgets($node->nid, TRUE, TRUE);

    if (!empty($widgets->snippets) && is_array($widgets->snippets)) {
      $snippet = reset($widgets->snippets);
    }
    else {
      // When there are no snippets we need at least its settings to still
      // work properly.
      $snippet_settings = $this->loadSnippetSettings();
      $snippet = array(
        'settings' => $snippet_settings,
      );
    }

    $sidebars = array();
    if (!empty($widgets->sidebars)) {
      foreach ($widgets->sidebars as $sidebar) {
        if (!empty($sidebar->id)) {
          $sidebars[$sidebar->id] = $sidebar;
        }
      }
    }

    $annotations = $this->loadAnnotations();

    return array(
      'snippet' => $snippet,
      'sidebars' => $sidebars,
      'annotations' => $annotations,
    );
  }

  /**
   * Returns JS settings required for the widgets.
   *
   * @param object $node
   *
   * @return array
   */
  public function buildWidgetsSettings($node) {
    $api_server = _contextly_server_url('api');
    $main_server = _contextly_server_url('main');
    $widgets_url = _contextly_server_url('widgets');
    $api_key = variable_get('contextly_api_key', array());
    $site_path = '';
    if (isset($api_key['appID'])) {
      $site_path = $api_key['appID'];
    }
    return array(
      'contextlyWidgets' => array(
        'nid' => $node->nid,
        'version' => CONTEXTLY_CLIENT_VERSION,
        'mode' => variable_get('contextly_mode', ''),
        'sitePath' => $site_path,
        'apiProviderURL' => $api_server . 'easy_xdm/cors/index.html',
        'mainProviderURL' => $main_server . 'easy_xdm/cors/index.html',
        'settingsURL' => $api_server . 'pagewidgets/get/',
        'pageEventsURL' => $api_server . 'siteevents/put/',
        'widgetStylesURL' => $widgets_url,
      ),
    );
  }

  public function loadAnnotations($active_only = TRUE) {
    // TODO Handle request paging (on lower API level).
    $response = ContextlyApi::getInstance()
      ->api('annotations', 'list')
      ->get();
    if (!empty($response->success)) {
      $list = array();
      if (!empty($response->list)) {
        if ($active_only) {
          foreach ($response->list as $annotation) {
            if ($annotation->status === 'active') {
              $list[] = $annotation;
            }
          }
        }
        else {
          // No need to filter, just use result.
          $list = array_values($response->list);
        }
      }

      // Normalize URL scheme to at least "http://".
      foreach ($list as &$annotation) {
        $url = $annotation->site_url;
        $scheme = @parse_url($url, PHP_URL_SCHEME);
        if ($scheme === FALSE || $scheme === NULL) {
          // Missing scheme, add default one and check once again to be sure we
          // made it better, and not worse.
          $url = 'http://' . $url;
          if (@parse_url($url, PHP_URL_SCHEME) === 'http') {
            $annotation->site_url = $url;
          }
          else {
            throw new ContextlyException(t('Malformed search tab URL: !url', array(
              '!url' => $annotation->site_url,
            )));
          }
        }
      }
      unset($annotation);

      // Add default "Web" tab to the end of the list with zero ID and empty URL.
      $list[] = array(
        'id' => '0',
        'site_name' => t('Web'),
        'site_url' => '',
        'status' => 'active',
      );

      return $list;
    }
    else {
      throw new ContextlyApiException(t('Unable to load site annotations list.'), $response);
    }
  }

  public function searchLinks($site_url, $query, $page, $per_page) {
    $extra_params = array();
    if (!empty($site_url)) {
      $extra_params = array(
        'url' => $site_url,
      );
    }

    $response = ContextlyApi::getInstance()
      ->api('search', 'list')
      ->param('query', $query)
      ->param('page', $page)
      ->param('per_page', $per_page)
      ->extraParams($extra_params)
      ->get();
    if (empty($response->success)) {
      throw new ContextlyApiException(t('Unable to preform search query.'), $response);
    }

    $next_page = $response->next_page;

    // API returns 10 first pages only.
    // TODO Move this limitation to the API server.
    if ($response->page >= 10) {
      $next_page = FALSE;
    }

    $result = array(
      'query' => $response->query,
      'page' => $response->page,
      'siteUrl' => !empty($response->url) ? $response->url : '',
      'list' => array_values($response->list),
      'nextPage' => $next_page,
    );

    return $result;
  }

  public function searchSidebars($sidebar_id, $query, $page, $per_page) {
    $response = ContextlyApi::getInstance()
      ->api('sidebars', 'search')
      ->searchParam('name', ContextlyApi::SEARCH_TYPE_LIKE_BOTH, $query)
      ->searchParam('id', ContextlyApi::SEARCH_TYPE_NOT_EQUAL, $sidebar_id)
      ->param('page', $page)
      ->param('per_page', $per_page)
      ->get();

    if (empty($response->success)) {
      throw new ContextlyApiException(t('Unable to perform sidebars search query'), $response);
    }

    if ($response->page < $response->pages) {
      $next_page = TRUE;
    }
    else {
      $next_page = FALSE;
    }

    $result = array(
      'query' => $query,
      'page' => $response->page,
      'list' => array_values($response->list),
      'nextPage' => $next_page,
    );

    return $result;
  }

  public function fetchUrlInfo($url) {
    $response = ContextlyApi::getInstance()
      ->api('urls', 'get')
      ->extraParams(array(
        'url' => $url,
      ))
      ->get();
    if (!empty($response->success) && !empty($response->entry)) {
      return $response->entry;
    }
    else {
      throw new ContextlyApiException(t('Unable to fetch URL info.'), $response);
    }
  }

  public function removeSnippet($snippet_id) {
    $response = ContextlyApi::getInstance()
      ->api('snippets', 'delete')
      ->param('id', $snippet_id)
      ->get();
    if (!empty($response->success)) {
      return TRUE;
    }
    else {
      throw new ContextlyApiException(t('Unable to remove the snippet.'), $response);
    }
  }

  public function removeSidebar($sidebar_id) {
    $response = ContextlyApi::getInstance()
      ->api('sidebars', 'delete')
      ->param('id', $sidebar_id)
      ->get();
    if (!empty($response->success)) {
      return TRUE;
    }
    else {
      throw new ContextlyApiException(t('Unable to remove the sidebar.'), $response);
    }
  }

  public function saveSnippet(array $data) {
    if (!isset($data['snippet_id'])) {
      // Create new snippet first to get its ID.
      $snippet_id = $this->putSnippet(array(
        'custom_id' => $data['custom_id'],
      ));
    }
    else {
      // Just update links of existing snippet.
      $snippet_id = $data['snippet_id'];
    }

    // Remove links.
    if (!empty($data['remove_links'])) {
      $this->removeWidgetLinks($snippet_id, $data['remove_links']);
    }

    // Add and update links.
    if (!empty($data['links'])) {
      $this->saveWidgetLinks($snippet_id, $data['links']);
    }

    // Load full updated snippet.
    $snippet = $this->loadSnippet($snippet_id, TRUE, TRUE);
    return $snippet;
  }

  public function addSnippetLink(array $data) {
    // Get default URL title if not passed.
    if (!isset($data['title'])) {
      $url_info = $this->fetchUrlInfo($data['url']);
      $data['title'] = $url_info->title;
    }

    if (!isset($data['snippet_id'])) {
      // Create new snippet first to get its ID.
      $snippet_id = $this->putSnippet(array(
        'custom_id' => $data['custom_id'],
      ));
    }
    else {
      // Just update links of existing snippet.
      $snippet_id = $data['snippet_id'];
    }

    // Build fake list with single link and save it. Saving method doesn't
    // change links that are not passed to it.
    $links = array();
    $links[] = array(
      'url' => $data['url'],
      'title' => $data['title'],
      'type' => $data['type'],
      'pos' => $data['pos'],
    );
    $this->saveWidgetLinks($snippet_id, $links);

    // Load full updated snippet.
    $snippet = $this->loadSnippet($snippet_id, TRUE, TRUE);
    return $snippet;
  }

  public function putSidebar($sidebar) {
    $response = ContextlyApi::getInstance()
      ->api('sidebars', 'put')
      ->extraParams($sidebar)
      ->get();
    if (!empty($response->success) && !empty($response->id)) {
      return $response->id;
    }
    else {
      throw new ContextlyApiException(t('Unable to send the sidebar to the Contextly.'), $response);
    }
  }

  public function saveSidebar(array $data) {
    // First, update/insert the sidebar itself.
    $sidebar_id = $this->putSidebar($data['sidebar']);

    // Remove links.
    if (!empty($data['remove_links'])) {
      $this->removeWidgetLinks($sidebar_id, $data['remove_links']);
    }

    // Add and update links.
    if (!empty($data['links'])) {
      $this->saveWidgetLinks($sidebar_id, $data['links']);
    }

    // Load full updated snippet.
    $sidebar = $this->loadSidebar($sidebar_id, TRUE, TRUE);
    return $sidebar;
  }

  protected function loadSnippet($snippet_id, $admin = FALSE, $editor = FALSE) {
    $response = ContextlyApi::getInstance()
      ->api('snippets', 'get')
      ->param('id', $snippet_id)
      ->param('admin', $admin ? 1 : 0)
      ->param('editor', $editor ? 1 : 0)
      ->get();
    if (!empty($response->success) && !empty($response->entry)) {
      return $response->entry;
    }
    else {
      throw new ContextlyApiException(t('Unable to load the snippet.'), $response);
    }
  }

  protected function loadSidebar($sidebar_id, $admin = FALSE, $editor = FALSE) {
    $response = ContextlyApi::getInstance()
      ->api('sidebars', 'get')
      ->param('id', $sidebar_id)
      ->param('admin', $admin ? 1 : 0)
      ->param('editor', $editor ? 1 : 0)
      ->get();
    if (!empty($response->success) && !empty($response->entry)) {
      return $response->entry;
    }
    else {
      throw new ContextlyApiException(t('Unable to load the sidebar.'), $response);
    }
  }

  protected function loadSnippetSettings() {
    $response = ContextlyApi::getInstance()
      ->api('widgetsettings', 'get')
      ->get();
    if (!empty($response->success) && !empty($response->entry)) {
      return $response->entry;
    }
    else {
      throw new ContextlyApiException(t('Unable to load the snippet settings.'), $response);
    }
  }

  protected function saveWidgetLinks($id, array $links) {
    foreach ($links as $link) {
      // Force title to be single line, collapse multiple spaces into one and
      // remove leading/trailing spaces.
      $link['title'] = trim(preg_replace(array(
        '/\r\n|\n|\r/',
        '/\s+/'
      ), ' ', $link['title']));

      $response = ContextlyApi::getInstance()
        ->api('links', 'put')
        ->extraParams(array('snippet_id' => $id) + $link)
        ->get();
      if (empty($response->success)) {
        throw new ContextlyApiException(t('Unable to update the widget link.'), $response);
      }
    }
  }

  protected function removeWidgetLinks($id, array $links) {
    foreach ($links as $link) {
      $response = ContextlyApi::getInstance()
        ->api('links', 'delete')
        ->param('id', $link['id'])
        ->get();
      if (empty($response->success)) {
        throw new ContextlyApiException(t('Unable to remove the widget link.'), $response);
      }
    }
  }

  protected function putSnippet($data) {
    $response = ContextlyApi::getInstance()
      ->api('snippets', 'put')
      ->extraParams($data)
      ->get();
    if (!empty($response->success) && !empty($response->id)) {
      return $response->id;
    }
    else {
      throw new ContextlyApiException(t('Unable to send the snippet to the Contextly.'), $response);
    }
  }

}

/**
 * Custom exception class to distinguish it from other kinds of exceptions.
 */
class ContextlyException extends Exception {}
