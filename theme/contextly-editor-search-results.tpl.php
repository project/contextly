<?php
/**
 * @file Editor search results template.
 *
 * @var string $editor_type
 *   Possible values:
 *   - snippet
 *   - sidebar
 *   - link
 */
?>
<ul class="search-tabs nav nav-tabs hidden">
  <?php if ($editor_type === 'sidebar') : ?>
    <li class="search-tab right" data-search-type="sidebars" data-site-url="">
      <a class="search-tab-link" href="#">
        <?php print t('Existing sidebars'); ?>
      </a>
    </li>
  <?php endif; ?>
  <li class="search-tab" data-search-type="links" data-site-url="" data-template="searchTab">
    <a class="search-tab-link" href="#"></a>
  </li>
</ul>
<div class="search-no-results hidden">
  <?php print t('No results.'); ?>
</div>
<ul class="search-results hidden">
  <li class="search-result clearfix" data-template="searchLinksResult">
    <div class="search-result-action-wrapper left cell">
      <?php if ($editor_type === 'link'): ?>
        <button class="website-add btn btn-mini btn-success">
          <i class="icon-ok"></i> <?php print t('Use'); ?>
        </button>
      <?php else: ?>
        <button class="website-add btn btn-mini">
          <i class="icon-arrow-right"></i> <?php print t('Add'); ?>
        </button>
      <?php endif; ?>
    </div>
    <div class="search-result-info cell">
      <h4 class="search-result-title"></h4>

      <p class="search-result-description"></p>

      <div class="website-url-wrapper">
        <a href="" class="website-link"
           title="<?php print check_plain(t('Preview')); ?>">
          <i class="icon-eye-open"></i> <span class="website-url"></span>
        </a>
      </div>
    </div>
    <div class="search-result-action-wrapper right cell">
      <?php if ($editor_type === 'link'): ?>
        <button class="website-add btn btn-mini btn-success">
          <i class="icon-ok"></i> <?php print t('Use'); ?>
        </button>
      <?php else: ?>
        <button class="website-add btn btn-mini">
          <i class="icon-arrow-right"></i> <?php print t('Add'); ?>
        </button>
      <?php endif; ?>
    </div>
  </li>
  <?php if ($editor_type === 'sidebar'): ?>
    <li class="search-result search-sidebars-result clearfix" data-template="searchSidebarsResult">
      <div class="search-result-action-wrapper left cell">
        <div class="btn-group">
          <button class="search-sidebar-add-all btn btn-mini">
            <i class="icon-arrow-right"></i> <?php print t('Add all'); ?>
          </button>
          <button class="search-sidebar-actions-toggle btn btn-mini dropdown-toggle">
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            <li>
              <a href="#" class="search-sidebar-add-all">
                <i class="icon-arrow-right"></i> <?php print t('Add all links'); ?>
              </a>
            </li>
            <li>
              <a href="#" class="search-sidebar-overwrite">
                <i class="icon-double-angle-right"></i> <?php print t('Overwrite whole sidebar'); ?>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="search-result-info cell">
        <h4 class="search-result-title"></h4>

        <p class="search-result-description"></p>

        <ul class="search-sidebar-content">
          <li class="search-sidebar-content-item" data-template="searchSidebarsContentItem">
            <a class="search-sidebar-link" href="">
              <i class="icon-eye-open"></i> <span class="search-sidebar-link-title"></span>
            </a>

            <button class="search-sidebar-add-single btn btn-mini" title="<?php print check_plain(t('Add link')); ?>">
              <i class="icon-arrow-right"></i>
            </button>
          </li>
        </ul>
        <div class="search-sidebar-links-toggles hidden">
          <button class="search-sidebar-links-expand search-sidebar-links-toggle hidden btn btn-mini btn-info">
            <i class="icon-caret-down"></i> <?php print t('Expand'); ?>
          </button>
          <button class="search-sidebar-links-collapse search-sidebar-links-toggle hidden btn btn-mini btn-info">
            <i class="icon-caret-up"></i> <?php print t('Colapse'); ?>
          </button>
        </div>
      </div>
      <div class="search-result-action-wrapper right cell">
        <div class="btn-group">
          <button class="search-sidebar-add-all btn btn-mini">
            <i class="icon-arrow-right"></i> <?php print t('Add all'); ?>
          </button>
          <button class="search-sidebar-actions-toggle btn btn-mini dropdown-toggle">
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            <li>
              <a href="#" class="search-sidebar-add-all">
                <i class="icon-arrow-right"></i> <?php print t('Add all links'); ?>
              </a>
            </li>
            <li>
              <a href="#" class="search-sidebar-overwrite">
                <i class="icon-double-angle-right"></i> <?php print t('Overwrite whole sidebar'); ?>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </li>
  <?php endif; ?>
</ul>
<ul class="search-pager pager hidden">
  <li class="previous disabled">
    <a href="#" class="search-prev"><i class="icon-angle-left"></i> <?php print t('Previous'); ?></a>
  </li>
  <li class="next">
    <a href="#" class="search-next"><?php print t('Next'); ?> <i class="icon-angle-right"></i></a>
  </li>
</ul>
