<!DOCTYPE html>
<html lang="<?php print $language->language; ?>">
<head>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="contextly-overlay">
  <?php print $content; ?>
  <div class="progress-indicator hidden"></div>
</body>
</html>
