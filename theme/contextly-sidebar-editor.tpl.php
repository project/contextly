<div class="container-fluid contextly-editor" data-editor-type="sidebar">
  <?php print $alert; ?>
  <div class="row-fluid">
    <div class="span8">
      <?php print $input_fields; ?>
      <?php print $search_results; ?>
    </div>
    <div class="span4 sidebar-right">
      <div class="sidebar-settings">
        <div class="control-group">
          <input class="sidebar-title" type="text"
                 placeholder="<?php print check_plain(t('Sidebar title')); ?>">
        </div>
        <div class="control-group">
          <textarea class="sidebar-description"
                    placeholder="<?php print check_plain(t('Sidebar description')); ?>"></textarea>
        </div>
        <div class="btn-group input-prepend sidebar-layout">
          <span class="add-on"><?php print t('Layout:'); ?></span>
          <button class="btn sidebar-layout-switch" data-layout="left">
            <?php print t('Left'); ?>
          </button>
          <button class="btn sidebar-layout-switch" data-layout="wide">
            <?php print t('Wide'); ?>
          </button>
          <button class="btn sidebar-layout-switch" data-layout="right">
            <?php print t('Right'); ?>
          </button>
        </div>

        <div class="sidebar-modal modal hidden" tabindex="-1">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <i class="icon-remove"></i>
            </button>
            <h3>
              <?php print t('Both sidebar title and description are empty!'); ?>
            </h3>
          </div>
          <div class="modal-body">
            <p>
              <?php print t('Would you like to add a title and/or description to the sidebar?'); ?>
            </p>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" data-dismiss="modal">
              <?php print t('Yes, return to the form'); ?>
            </button>
            <button class="sidebar-modal-confirm btn btn-warning">
              <?php print t('No, just save it'); ?>
            </button>
          </div>
        </div>
      </div>

      <div class="sidebar-result hidden">
        <h3 class="widget-title"><?php print t('Result:'); ?></h3>

        <ul class="section-links">
          <li class="section-link clearfix" data-template="sectionLink">
            <div class="handle"></div>
            <a href="javascript:" class="remove-link"
               title="<?php print check_plain(t('Remove the link')); ?>">
              <i class="icon-trash"></i>
            </a>
            <a href="" class="test-link"
               title="<?php print check_plain(t('Preview')); ?>">
              <i class="icon-eye-open"></i>
            </a>

            <div class="link-title">
              <textarea class="link-title-editor"></textarea>
              <div class="link-progress-indicator"></div>
            </div>
          </li>
        </ul>
      </div>
      <div class="dialog-actions">
        <button class="dialog-action action-remove btn btn-danger hidden"
                title="<?php print check_plain(t('Remove the sidebar')); ?>">
          <i class="icon-trash"></i> <?php print t('Remove'); ?>
        </button>
        <button class="dialog-action action-cancel btn btn-warning"
                title="<?php print check_plain(t('Reset all changes')); ?>">
          <i class="icon-remove"></i> <?php print t('Cancel'); ?>
        </button>
        <button class="dialog-action action-save btn btn-success hidden"
                title="<?php print check_plain(t('Save the sidebar')); ?>">
          <i class="icon-ok"></i> <?php print t('Save'); ?>
        </button>
      </div>
    </div>
  </div>
</div>
<?php print $url_preview; ?>
