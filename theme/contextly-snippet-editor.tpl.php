<div class="container-fluid contextly-editor" data-editor-type="snippet">
  <?php print $alert; ?>
  <div class="row-fluid">
    <div class="span8">
      <?php print $input_fields; ?>
      <?php print $search_results; ?>
    </div>
    <div class="span4 sidebar-right">
      <div class="snippet-result hidden">
        <h3 class="widget-title"><?php print t('Result:'); ?></h3>
        <div class="sections">
          <div class="section" data-template="section">
            <h4 class="section-title"></h4>
            <ul class="section-links">
              <li class="section-link clearfix" data-template="sectionLink">
                <div class="handle"></div>
                <a href="javascript:" class="remove-link"
                   title="<?php print check_plain(t('Remove the link')); ?>">
                  <i class="icon-trash"></i>
                </a>
                <a href="" class="test-link"
                   title="<?php print check_plain(t('Preview')); ?>">
                  <i class="icon-eye-open"></i>
                </a>

                <div class="link-title">
                  <textarea class="link-title-editor"></textarea>
                  <div class="link-progress-indicator"></div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="dialog-actions">
        <button class="dialog-action action-remove btn btn-danger hidden"
                title="<?php print check_plain(t('Remove the widget')); ?>">
          <i class="icon-trash"></i> <?php print t('Remove'); ?>
        </button>
        <button class="dialog-action action-cancel btn btn-warning"
                title="<?php print check_plain(t('Reset all changes')); ?>">
          <i class="icon-remove"></i> <?php print t('Cancel'); ?>
        </button>
        <button class="dialog-action action-save btn btn-success hidden"
                title="<?php print check_plain(t('Save the widget')); ?>">
          <i class="icon-ok"></i> <?php print t('Save'); ?>
        </button>
      </div>
    </div>
  </div>
</div>
<?php print $url_preview; ?>
