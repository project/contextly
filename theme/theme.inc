<?php

function _contextly_generate_empty_js_static() {
  // url() generates the prefix using hook_url_outbound_alter(). Instead of
  // running the hook_url_outbound_alter() again here, extract the prefix
  // from url().
  url('', array('prefix' => &$prefix));
  return array(
    'settings' => array(
      'data' => array(
        array('basePath' => base_path()),
        array('pathPrefix' => empty($prefix) ? '' : $prefix),
      ),
      'type' => 'nonexistent',
      'scope' => 'header',
      'group' => JS_LIBRARY,
      'every_page' => TRUE,
      'weight' => 0,
    ),
  );
}

function template_preprocess_contextly_overlay(&$vars) {
  // First, reset all added head tags, CSS, JS and libraries.
  $caches = array(
    'drupal_add_html_head',
    'drupal_add_js',
    'drupal_add_css',
    'drupal_add_library',
  );
  foreach ($caches as $cache) {
    drupal_static_reset($cache);
  }

  // Hack to avoid default JS on the overlay editor.
  $static_value =& drupal_static('drupal_add_js', array());
  $static_value = _contextly_generate_empty_js_static();
  unset($static_value);

  // Language.
  $vars['language'] = $GLOBALS['language'];
}

function template_process_contextly_overlay(&$vars) {
  $element = $vars['element'];

  // Now render children, so their CSS, JS and head tags are added to the page.
  $vars['content'] = drupal_render_children($element);

  // Grab added content, so we could render it to the template.
  $vars['head'] = drupal_get_html_head();
  $vars['styles'] = drupal_get_css();
  $vars['scripts'] = drupal_get_js();
}

function template_preprocess_contextly_editor(&$vars) {
  $vars['alert'] = theme('contextly_editor_alert');
  $vars['input_fields'] = theme('contextly_editor_input_fields', array(
    'editor_type' => $vars['editor_type'],
  ));
  $vars['search_results'] = theme('contextly_editor_search_results', array(
    'editor_type' => $vars['editor_type'],
  ));
  $vars['url_preview'] = theme('contextly_editor_url_preview', array(
    'editor_type' => $vars['editor_type'],
  ));
}
