<div class="input-fields control-group">
  <div class="input-prepend input-append inline">
    <span class="add-on"><i class="icon-search"></i></span>
    <input class="search-phrase" type="text"
           placeholder="<?php print check_plain(t('Search phrase')); ?>">
    <button class="search-submit btn btn-primary">
      <i class="icon-arrow-down"></i> <?php print t('Search'); ?>
    </button>
  </div>

  <span class="inputs-splitter">or</span>

  <div class="input-prepend input-append inline">
    <span class="add-on"><i class="icon-link"></i></span>
    <input class="link-url" type="text"
           placeholder="<?php print check_plain(t('http://paste-a-link-here.com')); ?>">
    <?php if ($editor_type === 'link'): ?>
      <button class="url-submit btn btn-success">
        <i class="icon-ok"></i> <?php print t('Use link'); ?>
      </button>
    <?php else: ?>
      <button class="url-submit btn">
        <i class="icon-arrow-right"></i> <?php print t('Add link'); ?>
      </button>
    <?php endif; ?>
  </div>
</div>