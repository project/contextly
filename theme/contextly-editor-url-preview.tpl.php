<?php
/**
 * @file Framed URL preview template.
 *
 * @var string $editor_type
 *   Possible values:
 *   - snippet
 *   - sidebar
 *   - link
 */
?>
<div class="url-preview-overlay hidden">
  <div class="url-preview-frame-wrapper">
    <iframe class="url-preview-frame" src="about:blank" frameBorder="0"></iframe>
  </div>
  <div class="url-preview-toolbar">
    <a href="javascript:" class="url-preview-link">
      <i class="icon-external-link"></i> <?php print t('View in new tab'); ?>
    </a>
    <div class="url-preview-buttons">
      <?php if (in_array($editor_type, array('snippet', 'sidebar'), TRUE)): ?>
        <button class="url-preview-remove btn btn-mini btn-danger hidden">
          <i class="icon-trash"></i> <?php print t('Remove link'); ?>
        </button>
      <?php endif; ?>

      <button class="url-preview-cancel btn btn-mini btn-warning">
        <i class="icon-remove"></i> <?php print t('Close preview'); ?>
      </button>

      <?php if ($editor_type === 'link'): ?>
        <button class="url-preview-confirm btn btn-mini btn-success hidden">
          <i class="icon-ok"></i> <?php print t('Use link'); ?>
        </button>
      <?php else: ?>
        <button class="url-preview-confirm btn btn-mini hidden">
          <i class="icon-arrow-right"></i> <?php print t('Add link'); ?>
        </button>
      <?php endif; ?>
    </div>
  </div>
</div>