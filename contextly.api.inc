<?php

/**
 * API client class for Contextly.
 *
 * @author Meshin Dmitry <0x7ffec@gmail.com>
 */
class ContextlyApi {

  const ACCESS_TOKEN_NAME = 'Contextly-Access-Token';
  const ACCESS_TOKEN_APP_ID_NAME = 'Contextly-App-ID';

  const SEARCH_TYPE_NOT_EQUAL = '!=';
  const SEARCH_TYPE_EQUAL = '=';
  const SEARCH_TYPE_GREATER = '>';
  const SEARCH_TYPE_LESS = '<';
  const SEARCH_TYPE_GREATER_EQUAL = '>=';
  const SEARCH_TYPE_LESS_EQUAL = '<=';
  const SEARCH_TYPE_LIKE = '~';
  const SEARCH_TYPE_LIKE_LEFT = '%~';
  const SEARCH_TYPE_LIKE_RIGHT = '~%';
  const SEARCH_TYPE_LIKE_BOTH = '%~%';
  const SEARCH_TYPE_REGEXP = 'regexp';

  const RESPONSE_SUCCESS = 200;
  const RESPONSE_CREATED = 201;
  const RESPONSE_ACCEPTED = 202;
  const RESPONSE_NO_CONTENT = 204;
  const RESPONSE_NOT_MODIFIED = 304;
  const RESPONSE_BAD_REQUEST = 400;
  const RESPONSE_UNAUTHORIZED = 401;
  const RESPONSE_FORBIDDEN = 403;
  const RESPONSE_NOT_FOUND = 404;
  const RESPONSE_NOT_ACCEPTABLE = 406;
  const RESPONSE_NOT_REGISTERED = 407;
  const RESPONSE_SUSPENDED = 408;
  const RESPONSE_TWITTER_ERROR = 700;

  /**
   * @var array
   */
  protected $options = array(
    'serverURL' => '',
    'authAPI' => 'auth',
    'authMethod' => 'auth',
    'appID' => '',
    'appSecret' => '',
    'requireSSL' => false,
  );

  /**
   * Headers to be sent.
   *
   * @var array
   */
  protected $headers = array(
    'Content-Type' => 'application/x-www-form-urlencoded',
  );

  /**
   * @var string
   */
  protected $method_name = null;

  /**
   * @var string
   */
  protected $api_name = null;

  /**
   * @var array
   */
  protected $_params = array();

  /**
   * @var array
   */
  protected $_extraParams = array();

  /**
   * @var ContextlySessionIsolated
   */
  protected $session = NULL;

  /**
   * @return ContextlyAPI
   */
  public static function getInstance() {
    static $instance = null;

    if (null === $instance) {
      $instance = new self(new ContextlySessionShared());
    }

    return $instance;
  }

  /**
   * Public constructor allowing to create API clients different from default one.
   *
   * @param ContextlySessionIsolated $session
   * @param array|null $key
   */
  public function __construct($session, $key = NULL) {
    $this->session = $session;

    if (!isset($key)) {
      $key = variable_get('contextly_api_key', array());
    }

    $options = $key + array(
      'serverURL' => _contextly_server_url('api'),
    );
    $this->setOptions($options);
  }

  /**
   * Performs HTTP call to the API server and returns parsed JSON response.
   *
   * @param string $api_name
   * @param string $method_name
   * @param array $params
   * @param array $post_data
   * @param array $headers
   *
   * @return mixed
   */
  protected function call($api_name, $method_name, array $params, array $post_data, array $headers = array()) {
    // Construct URL.
    $url = rtrim($this->options['serverURL'], "/");
    $url .= "/" . $api_name . "/" . $method_name . "/";

    // Append slash-separated parameters to URL.
    if (!empty($params)) {
      $url .= implode("/", $params) . "/";
    }

    // Add client version.
    $post_data['version'] = CONTEXTLY_CLIENT_VERSION;

    // Add default headers.
    $headers += $this->headers;

    // Add default referrer.
    if (isset($_SERVER['SERVER_NAME']) && !isset($headers['Referer'])) {
      $headers['Referer'] = $_SERVER['SERVER_NAME'];
    }

    // TODO requireSSL option support.
    $response = drupal_http_request($url, array(
      'method' => 'POST',
      'data' => http_build_query($post_data, NULL, '&'),
      'headers' => $headers,
    ));

    // TODO HTTP error handling!

    // Parse API response and check it.
    $result = json_decode($response->data);
    if ($result === NULL) {
      throw new ContextlyException(t("Unable to decode JSON response from server. Status: !code !message. Response body:\n!response", array(
        '!code' => $response->code,
        '!message' => $response->status_message,
        '!response' => $response->data,
      )));
    }

    return $result;
  }

  /**
   * @return mixed
   */
  public function get() {
    $this->connect();

    $headers = $this->headers;

    // Set auth headers with token.
    $access_token = $this->session->getAccessToken();
    $headers[self::ACCESS_TOKEN_NAME] = $access_token;
    $headers[self::ACCESS_TOKEN_APP_ID_NAME] = $this->options['appID'];

    $response = $this->call($this->api_name, $this->method_name, $this->_params, $this->_extraParams, $headers);

    $this->method_name = null;
    $this->api_name = null;
    $this->_params = array();
    $this->_extraParams = array();

    return $response;
  }

  /**
   * Returns current session.
   *
   * @return ContextlySessionIsolated|null
   */
  public function getSession() {
    return $this->session;
  }

  /**
   * @param $api_name
   * @param $method_name
   *
   * @return ContextlyApi
   */
  public function api($api_name, $method_name) {
    $this->method_name = $method_name;
    $this->api_name = $api_name;
    return $this;
  }

  /**
   * @param $name
   * @param $value
   *
   * @return ContextlyAPI
   */
  public function param($name, $value) {
    if ($name && $value) {
      $this->_params[] = (String) $name;
      $this->_params[] = (String) $value;
    }
    return $this;
  }

  /**
   * @param $name
   * @param $value
   *
   * @return ContextlyAPI
   */
  public function extraParam($name, $value) {
    if ($name && isset($value)) {
      $this->_extraParams[(String) $name] = $value;
    }
    return $this;
  }

  /**
   * @param array $params
   *
   * @return ContextlyAPI
   */
  public function params(array $params) {
    foreach ($params as $p_name => $p_value) {
      $this->param($p_name, $p_value);
    }

    return $this;
  }

  /**
   * @param array $extraparams
   *
   * @return ContextlyAPI
   */
  public function extraParams(array $extraparams) {
    foreach ($extraparams as $ep_name => $ep_value) {
      $this->extraParam($ep_name, $ep_value);
    }
    return $this;
  }

  /**
   * @param $column
   * @param $type
   * @param $value
   *
   * @return ContextlyAPI
   */
  public function searchParam($column, $type, $value) {

    if (!$column || !$type || !$value) {
      return $this;
    }

    $allowed_types = array(
      self::SEARCH_TYPE_NOT_EQUAL,
      self::SEARCH_TYPE_EQUAL,
      self::SEARCH_TYPE_GREATER,
      self::SEARCH_TYPE_LESS,
      self::SEARCH_TYPE_GREATER_EQUAL,
      self::SEARCH_TYPE_LESS_EQUAL,
      self::SEARCH_TYPE_LIKE,
      self::SEARCH_TYPE_LIKE_LEFT,
      self::SEARCH_TYPE_LIKE_RIGHT,
      self::SEARCH_TYPE_LIKE_BOTH,
      self::SEARCH_TYPE_REGEXP
    );

    if (!in_array($type, $allowed_types)) {
      return $this;
    }

    if (!isset($this->_extraParams['filters'])) {
      $this->_extraParams['filters'] = '';
    }

    $this->_extraParams['filters'] .= $column . ';' . $type . ';' . urlencode($value) . ';*';

    return $this;
  }

  /**
   * @param array $opts
   *
   * @return ContextlyAPI
   */
  public function setOptions(array $opts) {
    $this->options = array_merge($this->options, $opts);
    return $this;
  }


  /**
   * @return ContextlyAPI
   * @throws ContextlyException
   */
  public function connect() {
    $this->authorize();

    return $this;
  }

  /**
   * @throws ContextlyException
   */
  protected function authorize() {
    $token_raw = null;
    $response = null;

    // getting access token first from session. and check expiration
    if (!$this->session->isAuthorized()) {
      $auth_info = array(
        'appID' => $this->options['appID'],
        'appSecret' => $this->options['appSecret'],
      );
      $response = $this->call($this->options['authAPI'], $this->options['authMethod'], array(), $auth_info);

      if (isset($response->success) && isset($response->access_token)) {
        $token_raw = $response->access_token;
        $this->session->saveAccessToken($token_raw);
      }
    }
    else {
      $token_raw = $this->session->getAccessToken();
    }

    if (null == $token_raw || !$this->session->isAuthorized()) {
      $exception_message = t('Can not authorize with provided appID and appSecret.');
      throw new ContextlyApiException($exception_message, $response);
    }
  }

}

/**
 * Isolated session handler for Contextly API client.
 *
 * Stores access token in property.
 */
class ContextlySessionIsolated {

  const ACCESS_TOKEN_EXPIRE_RESERVE = 60;

  /**
   * Access token info.
   *
   * Possible values:
   * - NULL: token was not yet loaded or have been erased.
   * - FALSE: token loading failed.
   * - array: token was loaded from cache or saved successfully.
   *
   * @var null|bool|array
   */
  protected $token_info = NULL;

  /**
   * Checks if we still have not expired access token.
   *
   * @return bool
   */
  public function isAuthorized() {
    if (empty($this->token_info)) {
      return FALSE;
    }

    // Drupal cache returns expired values until the next cron run. Check
    // expiration at this point. Make sure we have some expiration reserve to
    // perform operations using this token before it will be expired. Also, the
    // time difference is possible between local and remote servers.
    if ($this->token_info['expires'] < time() + self::ACCESS_TOKEN_EXPIRE_RESERVE) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Removes token data from cache.
   */
  public function clearTokenData() {
    $this->token_info = NULL;
  }

  /**
   * Returns raw access token or NULL if it was not generated or has been expired.
   *
   * @return null|string
   */
  public function getAccessToken() {
    if (!empty($this->token_info)) {
      return implode('-', $this->token_info);
    }
    else {
      return NULL;
    }
  }

  public function parseAccessToken($token) {
    if (!$token) {
      return FALSE;
    }

    // Token has format "asdfasdf-123123", where first part is signature and the
    // second is expiration timestamp.
    if (!preg_match('/^([^\-]+)\-(\d+)$/', $token, $matches)) {
      return FALSE;
    }

    // Convert matches into associative array for future use and save access
    // token to cache.
    $token_info = array(
      'token' => $matches[1],
      'expires' => $matches[2],
    );
    return $token_info;
  }

  /**
   * Saves access token to a permanent storage for future use.
   *
   * @param $token string
   *
   * @return bool
   */
  public function saveAccessToken($token) {
    $token_info = $this->parseAccessToken($token);
    if (!$token_info) {
      return FALSE;
    }

    $this->token_info = $token_info;
    return TRUE;
  }

}

class ContextlySessionShared extends ContextlySessionIsolated {

  const ACCESS_TOKEN_CID = 'contextly:access-token';
  const ACCESS_TOKEN_CACHE_BIN = 'cache';

  public function isAuthorized() {
    $this->loadAccessTokenIfNecessary();
    return parent::isAuthorized();
  }

  public function clearTokenData() {
    cache_clear_all(self::ACCESS_TOKEN_CID, self::ACCESS_TOKEN_CACHE_BIN);
    parent::clearTokenData();
  }

  /**
   * Loads access token info from cache if not yet tried.
   */
  protected function loadAccessTokenIfNecessary() {
    if (isset($this->token_info)) {
      return;
    }

    $cache = cache_get(self::ACCESS_TOKEN_CID, self::ACCESS_TOKEN_CACHE_BIN);
    if (!$cache) {
      $this->token_info = FALSE;
      return;
    }

    $this->token_info = $cache->data;
  }

  public function getAccessToken() {
    $this->loadAccessTokenIfNecessary();
    return parent::getAccessToken();
  }

  public function saveAccessToken($token) {
    $parsing_successful = parent::saveAccessToken($token);
    if ($parsing_successful) {
      cache_set(self::ACCESS_TOKEN_CID, $this->token_info, self::ACCESS_TOKEN_CACHE_BIN, $this->token_info['expires']);
    }
    return $parsing_successful;
  }

}

/**
 * API Exceptions.
 */
class ContextlyApiException extends ContextlyException {

  public function __construct($message = '', $response = NULL) {
    $messages = array();
    if (!empty($message)) {
      $messages[] = $message;
    }

    if (!isset($response)) {
      $messages[] = t('Response from server is empty.');
    }

    if (isset($response->error)) {
      $messages[] = t('Message from server: @error', array(
        '@error' => $response->error,
      ));
    }

    $exception_code = 400;
    if (isset($response->error_code)) {
      $exception_code = $response->error_code;
    }

    parent::__construct(implode(' ', $messages), $exception_code);
  }
}
